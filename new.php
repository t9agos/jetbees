<?php
/**************************************************************************************************
| Fiverr Script
| http://www.fiverrscript.com
| webmaster@fiverrscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.fiverrscript.com/eula.html and to be bound by it.
|
| Copyright (c) FiverrScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	

		function randomPassword() {
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 60; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
		}
		
		//JetBees: adicionado campo token na query. Original era apenas o level 
		$query = "select addtime, email, token from members where USERID='".mysql_real_escape_string($_SESSION['USERID'])."'"; 
		$executequery=$conn->execute($query);
		
		//JetBees *****************************
		$token = $executequery->fields['token'];
		STemplate::assign('token',$token);
		
		//Criptograva o id do usuário
		$addtime = $executequery->fields['addtime'];
		$email = $executequery->fields['email'];
		$user_id = $_SESSION['USERID'];
		$hash = randomPassword();
		
		

		
		require 'mercadopago/ConexaoPedidos.class.php';
	
		$pdo = new ConexaoPedidos();	
		
		//Guarda o id do usuário em uma tabela que terá um registro temporário
		$sql = "INSERT INTO authorize (user_id, hash) VALUES (:user_id, :hash)";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":user_id", $_SESSION['USERID']);
		$stmt->bindParam(":hash", $hash);
		$stmt->execute();
		
		

		$sql = "SELECT id, hash FROM authorize WHERE user_id = :user_id";
						
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":user_id", $_SESSION['USERID']);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_ASSOC);
		
		
		$id = $resultado['id'];
		$hash = $resultado['hash'];
		
		STemplate::assign('state', base64_encode($id."jetbees".$hash));	
		//*************************************



	$allowposting = "1";
	$vonly = $config['vonly'];
	if($vonly == "1")
	{
		$uverified = $_SESSION['VERIFIED'];
		if($uverified != "1")
		{
			$allowposting = "0";
		}
	}
	if($config['enable_levels'] == "1" && $config['price_mode'] == "3")
	{
		$timecheck = time() - 86400;
		$query = "select count(*) as total from posts where USERID='".mysql_real_escape_string($_SESSION['USERID'])."' AND time_added>='".mysql_real_escape_string($timecheck)."'"; 
		$executequery=$conn->execute($query);
		$lastjobs = $executequery->fields['total']+0;
		 
		$query = "select level from members where USERID='".mysql_real_escape_string($_SESSION['USERID'])."'"; 
		$executequery=$conn->execute($query);
		
		$mlevel = intval($executequery->fields['level']);
		if($mlevel == "3")
		{
			$jlimit = $config['level3job'];
		}
		elseif($mlevel == "2")
		{
			$jlimit = $config['level2job'];
		}
		elseif($mlevel == "1")
		{
			$jlimit = $config['level1job'];
		}
		else
		{
			$jlimit = "";
		}
		if($jlimit != "")
		{
			if($lastjobs >= $jlimit)
			{
				$allowposting = "2";
			}
		}
	}
	
	if($allowposting == "1")
	{
		if($_POST['subform'] == "1")
		{
			$gtitle = cleanit($_REQUEST['gtitle']);	
			$gcat = intval(cleanit($_REQUEST['gcat']));
			$gdesc = htmlentities(strip_tags(stripslashes($_REQUEST['gdesc']), '<p><i><strong><br><font><span><em><ol><li>'), ENT_COMPAT, "UTF-8");	
			$ginst = cleanit($_REQUEST['ginst']);	
			$gtags = cleanit($_REQUEST['gtags']);
			$gtags = str_replace(".", "", $gtags);	
			$gdays = intval(cleanit($_REQUEST['gdays']));
			$gyoutube = cleanit($_REQUEST['gyoutube']);
			$shipme = intval(cleanit($_REQUEST['shipme']));
			$multipleme = intval(cleanit($_REQUEST['multipleme']));
			$extrasme = intval(cleanit($_REQUEST['extrasme']));
			$instanturl = cleanit($_REQUEST['instanturl']);
			if($gtitle == "")
			{
				$error = "<li>".$lang['92']."</li>";
			}
			elseif(strlen($gtitle) > 200)
			{
				$error = "<li>".$lang['509']."</li>";
			}
			if($shipme == "1")
			{
				include("new_ship.scriptolution.com.php");
			}
			elseif($multipleme == "1")
			{
				$multiplemax = intval(cleanit($_REQUEST['multiplemax']));
				if($multiplemax > 0)
				{
					$scriptolution_add_multiple = ", scriptolution_add_multiple='".mysql_real_escape_string($multiplemax)."'";	
				}
				$fiverrscriptdotcom = "1";
			}
			elseif($extrasme == "1")
			{
				$scriptolution_process_extras = "1";
				$fiverrscriptdotcom = "1";
			}
			else
			{
				$fiverrscriptdotcom = "1";	
			}
			if($fiverrscriptdotcom == "1")
			{
				if($config['price_mode'] == "1")
				{
					$price = intval(cleanit($_REQUEST['gprice']));
					if($price == "0")
					{
						$error = "<li>".$lang['127']."</li>";
					}
					$comper = intval($config['commission_percent']);
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2, '.', '');
				}
				elseif($config['price_mode'] == "3")
				{
					$PACID = intval(cleanit($_REQUEST['gprice']));
					$query = "select pprice,pcom from packs where ID='".mysql_real_escape_string($PACID)."'"; 
					$executequery=$conn->execute($query);
					$price = intval(cleanit($executequery->fields['pprice']));
					$comper = intval(cleanit($executequery->fields['pcom']));
					if($price == "0")
					{
						$error = "<li>".$lang['435']."</li>";
					}
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2, '.', '');
					
				}
				elseif($config['price_mode'] == "2")
				{
					$price = intval($config['price']);
					$comper = intval($config['commission_percent']);
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2, '.', '');
				}
				else
				{
					$price = intval($config['price']);
					$mysetc = $config['commission'];
					$ctp = number_format($mysetc, 2);
				}
			}
			if($gcat == "0")
			{
				$error .= "<li>".$lang['93']."</li>";
			}
			if($gdesc == "")
			{
				$error .= "<li>".$lang['94']."</li>";
			}
			elseif(strlen($gdesc) > 50000)
			{
				$error = "<li>".$lang['593']."</li>";
			}
			if($ginst == "")
			{
				$error .= "<li>".$lang['95']."</li>";
			}
			if($gtags == "")
			{
				$error .= "<li>".$lang['96']."</li>";
			}

			if(scriptolution_banned_words_chk($gtitle))
			{
				$error .= "<li>".$lang['556']."</li>";
			}
			if(scriptolution_banned_words_chk($gdesc))
			{
				$error .= "<li>".$lang['586']."</li>";
			}
			if(scriptolution_banned_words_chk($gtags))
			{
				$error .= "<li>".$lang['587']."</li>";
			}
			if(scriptolution_banned_words_chk($ginst))
			{
				$error .= "<li>".$lang['588']."</li>";
			}

			if($gdays == "0")
			{
				if($instanturl=="" && $_FILES['instantfile']['tmp_name']=="")
				{
					$error .= "<li>".$lang['97']."</li>";
				}
			}
			if($instanturl!="" || $_FILES['instantfile']['tmp_name']!="")
			{
				include("new_instant.scriptolution.com.php");
			}
			$gphoto = $_FILES['gphoto']['tmp_name'];
			if($gphoto != "")
			{
				$ext = substr(strrchr($_FILES['gphoto']['name'], '.'), 1);
				$ext2 = strtolower($ext);
				if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "gif" || $ext2 == "png")
				{
					$theimageinfo = getimagesize($gphoto);
					if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
					{
						$error .= "<li>".$lang['100']."</li>";
					}
				}
				else
				{
					$error .= "<li>".$lang['100']."</li>";
				}
			}
			else
			{
				$error .= "<li>".$lang['101']."</li>";
			}
			if($gyoutube != "")
			{
				$gyoutube = str_replace("https://", "http://", $gyoutube);
				$pos = strpos($gyoutube, "http://www.youtube.com/watch?v=");
				$posb = strpos($gyoutube, "http://www.youtu.be/");
				$posc = strpos($gyoutube, "http://youtu.be/");
				if ($pos === false)
				{
					if ($posb === false)
					{
						if ($posc === false)
						{
							$error .= "<li>".$lang['133']."</li>";
						}
					}
				}
			}
			if($error == "")
			{			
				$approve_stories = $config['approve_stories'];
				if($approve_stories == "1")
				{
					$active = "0";
				}
				else
				{
					$active = "1";
				}
				$query="INSERT INTO posts SET USERID='".mysql_real_escape_string($_SESSION['USERID'])."', gtitle='".mysql_real_escape_string($gtitle)."',gtags='".mysql_real_escape_string($gtags)."', gdesc='".mysql_real_escape_string($gdesc)."', ginst='".mysql_real_escape_string($ginst)."', days='".mysql_real_escape_string($gdays)."', youtube='".mysql_real_escape_string($gyoutube)."', category='".mysql_real_escape_string($gcat)."', price='".mysql_real_escape_string($price)."', time_added='".time()."', date_added='".date("Y-m-d")."', pip='".$_SERVER['REMOTE_ADDR']."', active='$active', ctp='".mysql_real_escape_string($ctp)."' $scriptolution_add_multiple $addship $scriptolution_add_instant";
				$result=$conn->execute($query);
				$pid = mysql_insert_id();
				
				if($scriptolution_process_extras == "1")
				{
					include("new_extras.scriptolution.com.php");
				}
				if($gphoto != "")
				{
					$thepp = $pid."-1";
					if($theimageinfo[2] == 1)
					{
						$thepp .= ".gif";
					}
					elseif($theimageinfo[2] == 2)
					{
						$thepp .= ".jpg";
					}
					elseif($theimageinfo[2] == 3)
					{
						$thepp .= ".png";
					}
					if($error == "")
					{
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						move_uploaded_file($gphoto, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "380", "265", false, $config['pdir']."/t/".$thepp);
						do_resize_image($myvideoimgnew, "102", "72", false, $config['pdir']."/t2/".$thepp);
						do_resize_image($myvideoimgnew, "678", "458", false, $config['pdir']."/t3/".$thepp);
						do_resize_image($myvideoimgnew, "214", "132", false, $config['pdir']."/t4/".$thepp);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE posts SET p1='$thepp' WHERE PID='".mysql_real_escape_string($pid)."'";
							$conn->execute($query);
						}
					}
				}
				$gphoto2 = $_FILES['gphoto2']['tmp_name'];
				if($gphoto2 != "")
				{
					$ext = substr(strrchr($_FILES['gphoto2']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "gif" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto2);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid."-2";
						if($theimageinfo[2] == 1)
						{
							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
	
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						move_uploaded_file($gphoto2, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "380", "265", false, $config['pdir']."/t/".$thepp);
						do_resize_image($myvideoimgnew, "102", "72", false, $config['pdir']."/t2/".$thepp);
						do_resize_image($myvideoimgnew, "678", "458", false, $config['pdir']."/t3/".$thepp);
						do_resize_image($myvideoimgnew, "214", "132", false, $config['pdir']."/t4/".$thepp);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE posts SET p2='$thepp' WHERE PID='".mysql_real_escape_string($pid)."'";
							$conn->execute($query);
						}
					}
				}
				$gphoto3 = $_FILES['gphoto3']['tmp_name'];
				if($gphoto3 != "")
				{
					$ext = substr(strrchr($_FILES['gphoto3']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "gif" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto3);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid."-3";
						if($theimageinfo[2] == 1)
						{
							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
	
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						move_uploaded_file($gphoto3, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "380", "265", false, $config['pdir']."/t/".$thepp);
						do_resize_image($myvideoimgnew, "102", "72", false, $config['pdir']."/t2/".$thepp);
						do_resize_image($myvideoimgnew, "678", "458", false, $config['pdir']."/t3/".$thepp);
						do_resize_image($myvideoimgnew, "214", "132", false, $config['pdir']."/t4/".$thepp);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE posts SET p3='$thepp' WHERE PID='".mysql_real_escape_string($pid)."'";
							$conn->execute($query);
						}
					}
				}
				if($approve_stories == "1")
				{
					$message = $lang['126'];
					if($config['scriptolution_notify_gigval'] == "1")
					{
						$scriptolution_notify_gigval_email = $config['scriptolution_notify_gigval_email'];
						if($scriptolution_notify_gigval_email != "")
						{
							$sendername = $config['site_name'];
							$from = $config['site_email'];
							$subject = $lang['553'];
							$sendmailbody = $lang['554']."<br><br>";
							$sendmailbody .= $lang['555']."<br>";
							$sendmailbody .= "<a href=".$config['baseurl']."/administrator/gigs_validate.php>".$config['baseurl']."/administrator/gigs_validate.php</a><br><br>";
							$sendmailbody .= $lang['23'].",<br>".stripslashes($sendername);
							mailme($scriptolution_notify_gigval_email,$sendername,$from,$subject,$sendmailbody,$bcc="");
						}
					}					
				}
				else
				{
					$gurl = $config['baseurl']."/".get_cat_seo($gcat)."/".$pid."/".seo_clean_titles($gtitle);
					$feurl = $config['baseurl']."/feature?id=".$pid;
					header("Location:$feurl");exit;
				}
			}
			else
			{	
				STemplate::assign('gtitle',$gtitle);
				STemplate::assign('gcat',$gcat);
				STemplate::assign('gdesc',$gdesc);
				STemplate::assign('ginst',$ginst);
				STemplate::assign('gtags',$gtags);
				STemplate::assign('gdays',$gdays);
				STemplate::assign('gyoutube',$gyoutube);
			}
		}
		else
		{
			$gtitle = cleanit($_REQUEST['quicktitle']);
			STemplate::assign('gtitle',$gtitle);
		}
		$templateselect = "new.tpl";
	}
	elseif($allowposting == "2")
	{
		$error = $lang['498'];
		$templateselect = "new2.tpl";
	}
	else
	{
		$error = $lang['445'];
		$templateselect = "new2.tpl";
	}
	$pagetitle = $lang['61'];
	STemplate::assign('pagetitle',$pagetitle);
}
else
{
	header("Location:$config[baseurl]/login");exit;
}

//TEMPLATES BEGIN
STemplate::assign('sm0',"1");
STemplate::assign('error',$error);
STemplate::assign('message',$message);
STemplate::display('scriptolution_header.tpl');
STemplate::display($templateselect);
STemplate::display('scriptolution_footer_nobottom.tpl');
//TEMPLATES END
?>