<?php
#-----------------------------------------------------------------------
#Vers„o: 1.0
#Data: 08/12/2006
#Nome: Diego Hellas
#E-mail: diegohellas@gmail.com
#DescriÁ„o: Gera um thumb da imagem se gerar uma nova imagem no HD
#Modo de uso: <img src="gera_thumb.inc.php?imagem=CAMINHO_IMAGEM&x=LARGURA&y=ALTURA">
#Creditos: Eu n„o desenvolvi toda a ideia do script, eu peguei um script como base e fui
#          alterando para que ele se adquase a minha necessidade.
#          O script Original È do Leonardo Giori(BOZO - Bozo@gambiarra.com.br)
#          links de referÍncia: 
#           - http://www.php5.com.br/index.php?php=_scripts/mostra&id_s=38
#           - http://www.phpbrasil.com/scripts/script.php/id/2642
#----------------------------------------------------------------------- 


#recebe o caminho para a imagem
$imagem = (isset($_GET["imagem"])) ? $_GET['imagem'] : "";

#verifica se foi recebida alguma imagem
if(!(empty($imagem)))
{
	#pega os tamanhos originais da imagem
	$imgPropriedades = getimagesize($imagem);
	$xOriginal = $imgPropriedades[0];
	$yOriginal = $imgPropriedades[1];
	
	#recebe a qualidade que ser· regado o thumb, quanto maior a qualidade maior ser· o tamanho em kb do arquigo gerado
	$qualidade = (isset($_GET['qualidade'])) ? $_GET['qualidade'] : 75;
	
	
	#verifica quais os par‚metros de tamanho forampassados
	if(isset($_GET['x']) && isset($_GET['y']))
	{
		#se foi passado o x e o y a thumb ser· gerado no tamanho fixo
		$x = $_GET['x'];
		$y = $_GET['y'];
	}	
	elseif(isset($_GET["x"]) && !(isset($_GET["y"])))
	{
		#se foi passado somente o x, o y ser· proporcional ao x
		$x = $_GET['x'];
		$y = ($x / $xOriginal) * $yOriginal;
	}
	elseif(!(isset($_GET["x"])) && isset($_GET["y"]))
	{
		#se foi passado somente o y, o x ser· proporcional ao y
		$y = $_GET['y'];
		$x = ($y / $yOriginal) * $xOriginal;		
	}
	elseif(!(isset($_GET["x"])) && !(isset($_GET["y"])))
	{
		#se n„o for passado nenhum par‚metro ele ir· gerar um thum com os tamanhos reias da imagem(sem sentido nÈ...)
		$x = $imgXY[0];
		$y = $imgXY[1];
	}
		
	
	#verifica qual a extenÁ„o da imgem
	$extensao = explode(".",$imagem);
	$extensao = strtoupper(end($extensao));
	if(($extensao=="JPG") || ($extensao=="JPEG") || ($extensao=="jpg") || ($extensao=="jpeg"))
	{
	    $tipo = "JPEG";
	}
	elseif($extensao=="GIF" || $extensao=="gif")
	{
	    $tipo = "GIF";
	}
	elseif($extensao=="PNG" || $extensao=="png")
	{
	    $tipo = "PNG";
	}
	else
	{
	    $tipo = "NULL";
	}
		
	#cria uma nova imagem
	$imagemNova = imagecreatetruecolor($x, $y);	
	$CriarImagemDe= 'ImageCreateFrom'.$tipo;
	$image = $CriarImagemDe($imagem);	
	
	#redimenciona a imgem para thumb
	imagecopyresampled($imagemNova, $image, 0, 0, 0, 0, $x, $y, $xOriginal,$yOriginal);
	if($tipo == "JPEG")
	{
		imagejpeg($imagemNova);
	}
	elseif($tipo == "GIF")
	{
		imagegif($imagemNova);
	}
	elseif($tipo == "PNG")
	{
	 	imagepng($imagemNova);
	}
	
	
	#escreve a imagem na tela
	Header("Content-disposition: filename=$imagem");
	Header("Content-Type: image/$tipo");
	
	#apaga as imagens
	ImageInterlace($image,1);
	ImageDestroy($imagemNova);
}
?>