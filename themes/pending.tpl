{include file="scriptolution_error7.tpl"}                          
<div class="bodybg scriptolutionpaddingtop15 scriptolutionopages">
	<div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth842 gray">
		<div class="inner-wrapper scriptolutionwidth842">
			<div class="left-side scriptolutionwidth842">
				<div class="whiteBox twoHalfs padding0 scriptolutionwidth800">
                    
                    
                    <div id="scriptolutionOrderingForm" class="scriptolutionpadding20"> 
                        <h2 style="color:red;"><strong>Pagamento pendente</strong></h2>
                        <br />
                        <h4>Seu pedido foi registrado, porém a operadora ainda não liberou o pagamento.</h4>
                        <br />
                        <h4>Assim que a liberação for confirmada, você receberá um email com as informações do vendedor e as instruções para prosseguir com o pedido.</h4><br />
                        
                        <a class="btn btn-primary" href="https://www.jetbees.com" role="button">Ver mais serviços</a>
                                                  
                    </div>
                    
                    
					<div class="clear"></div>
				</div>
			</div>			
			<div class="clear"></div>
		</div>   
	</div>
</div>
<div id="scriptolutionnobottom">
    <div class="centerwrap footertop">
        <div class="footerbg gray scriptolutionfooter842"></div>
    </div>
</div>