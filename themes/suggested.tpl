{include file="scriptolution_error7.tpl"}
<div class="bodybg scriptolutionpaddingtop15">
	<div class="whitebody scriptolutionpaddingtop30 gray">
		<div class="inner-wrapper">
			<div class="clear"></div>
			<div class="left-side">
				<div class="whiteBox twoHalfs padding15 scriptolutionshop">
					<h1>{$lang116}</h1>
					<div class="whiteBox inside">
                        <form id="scriptolutionPostForm" action="{$baseurl}/suggested" method="POST">
                        <div class="iwill">
                            <div class="iwill-holder">
                                <div class="txt">{$lang117} </div>
                                <div class="f"><input class="text" type="text" value="" maxlength="80" name="sugcontent" /></div>
                                <div style="clear:both; padding-top:5px;"></div>
                                <div class="scriptolutionin">{$lang119}</div>
                                <select class="text" name="sugcat" style="display:inline-block !important">
                                {insert name=get_categories assign=c}
                                {section name=i loop=$c}
                                <option value="{$c[i].CATID|stripslashes}">{$c[i].name|stripslashes}</option>
                                {/section}
                                </select>
                                <div style="clear:both; padding-top:5px;"></div>
                                <div><input type="submit" value="{$lang118}" class="scriptolutionbluebutton" /></div>
                            </div>
                        </div>
                        <input type="hidden" name="sugsub" value="1" />
                        </form>
					</div>
                    <h1>{$lang496}</h1>
					<div class="db-main-table">
						<table>
							<thead>
								<tr>
									<td style="text-align:left;">{$lang36}</td>
                                    <td style="text-align:left;">{$lang580}</td>
                                    <td>{$lang581}</td>
								</tr>
							</thead>
							<tbody>
                            {if $posts|@count eq "0"}
                            <tr>
                                <td colspan="4">
                                {$lang583}
                                </td>
                            </tr>
                            {else}
                            	{section name=i loop=$posts}
                                {insert name=seo_clean_titles assign=title value=a title=$posts[i].gtitle}
								<tr>
									<td class="status-star">
                                    	<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes|truncate:15:"...":true}</a>
									</td>
									<td class="ellipsis-wrap">
										<div class="ellipsissuggest"> 
                                        	{$lang117}:<br />
                                        	{$posts[i].want|stripslashes|mb_truncate:200:"...":'UTF-8'}
										</div>
									</td>
                                    <td class="actionstatus">
                                    	{if $smarty.session.USERID ne $posts[i].USERID}
                                            {if $smarty.session.USERID GT "0"}
                                            <a class="ascriptolutiongreenbutton" href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$posts[i].username|stripslashes}{$cvseo}">{$lang582}</a>
                                            {else}
                                            <a class="ascriptolutiongreenbutton" href="{$baseurl}/login">{$lang582}</a>
                                            {/if}
                                        {else}
                                        	<form action="" method="post">
                                            <input type="hidden" name="sug" value="{$posts[i].WID}" />
                                            <input type="hidden" name="del" value="1" />
                                            <input type="submit" value="{$lang185}" class="ascriptolutionredbutton" />
                                            </form>
                                        {/if}
                                    </td>
								</tr>
                                {/section}
							{/if}
							</tbody>
						</table>
					</div>
					<div class="clear"></div>	
                    
                    <div align="center">
                        <div class="paging">
                            <div class="p1">
                                <ul>
                                    {$pagelinks}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>			
				</div>
                
                
                
			</div>
			<div class="right-side">
				<div class="sidebarBlock">
					<h3>{$lang577|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/new" class="greenBtn"><span>{$lang55}</span></a></li>
					</ul>
				</div>
                {if $smarty.session.USERID GT "0"}
				<div class="sidebarBlock">
					<h2>{$lang33|upper}</h2>
					<ul>
						<li><a href="{$baseurl}/orders">{$lang157}</a></li>
					</ul>
				</div>
				<div class="sidebarBlock">
					<h3>{$lang578|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/balance">{$lang159}</a></li>
					</ul>
				</div>
                {/if}
                <div class="sidebarBlock noBorder">
					<h3>{$lang496|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/mysuggestions">{$lang511}</a></li>
                        <li><a href="{$baseurl}/suggested">{$lang579}</a></li>
					</ul>
				</div>	
			</div>            	
		</div>    
	</div>
</div>