<script src="{$baseurl}/js/mainscriptolution.js" type="text/javascript"></script>
<script src="{$baseurl}/js/jquery.qtip-1.0.0-rc3.js" type="text/javascript"></script> 
<script src="{$baseurl}/js/jquery.corner.js" type="text/javascript"></script> 
<script src="{$baseurl}/js/indexes.js" type="text/javascript"></script> 
{literal}
<style>
.songslist h1 {
  float: left;
  width: 100%;
  font-size: 39px;
  font-family: 'latobold', sans-serif;
  color: #424242;
  margin-bottom: 15px;
}
</style>
{/literal}
{include file="scriptolution_error7.tpl"}
<div class="bodybg scriptolutionpaddingtop15 scriptolutionbookmarks">
	<div class="whitebody scriptolutionpaddingtop30 gray">
		<div class="inner-wrapper">
			<div class="clear"></div>
			<div class="left-side">

                <div class="songslist">
                    <h1>{$lang30}</h1>
                    <div class="cusongslist">
                        {include file="scriptolution_bit_last3_my.tpl"}                
                        <div class="clear"></div>
                    </div>
                    <div align="center">
                        <div class="paging">
                            <div class="p1">
                                <ul>
                                    {$pagelinks}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="clear" style="padding-bottom:20px;"></div>
                </div>
  
			</div>
			<div class="right-side">
				<div class="sidebarBlock">
					<h3>{$lang577|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/new" class="greenBtn"><span>{$lang55}</span></a></li>
					</ul>
				</div>
				<div class="sidebarBlock">
					<h2>{$lang33|upper}</h2>
					<ul>
						<li><a href="{$baseurl}/manage_gigs">{$lang153}</a></li>
                        <li><a href="{$baseurl}/manage_orders">{$lang154}</a></li>
					</ul>
				</div>
				<div class="sidebarBlock">
					<h3>{$lang578|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/balance?tab=sales">{$lang155}</a></li>
                        <li><a href="{$baseurl}/purchases">{$lang461}</a></li>
					</ul>
				</div>
                <div class="sidebarBlock noBorder">
					<h3>{$lang496|upper}</h3>
					<ul>
						<li><a href="{$baseurl}/mysuggestions">{$lang511}</a></li>
                        <li><a href="{$baseurl}/suggested">{$lang579}</a></li>
					</ul>
				</div>				
			</div>
		</div>    
	</div>
</div>