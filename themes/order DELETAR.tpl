{include file="scriptolution_error7.tpl"}  

   <!-- JetBees -->
   {assign var=funds value="0"}
   {assign var=afunds value="0"}
   <!-- /JetBees -->

{if $enable_paypal eq "1" AND $enable_alertpay eq "0" AND $funds LT $p.totalprice AND $afunds LT $p.totalprice}
{literal}
<script type="text/javascript"> 
$(document).ready(function(){
	$('#fazer_pagamento').click( function(){
		$('#paypal_form').submit();
	});
});
//$(document).ready( function() {
//    $('#paypal_form').submit();
//});
</script>
{/literal}
{elseif $enable_paypal eq "0" AND $enable_alertpay eq "1" AND $funds LT $p.totalprice AND $afunds LT $p.totalprice}
{literal}
<script type="text/javascript"> 
$(document).ready( function() {
    $('#alertpay_form').submit();
});
</script>
{/literal}
{/if}

<form action="mercadopago/pedido.php" method="post" id="paypal_form" name="paypal_form">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="{$paypal_email}">
<input type="hidden" name="item_name" value="#{$p.PID|stripslashes} - {$p.gtitle|stripslashes}">
<input type="hidden" name="item_number" value="{$p.IID|stripslashes}">
<input type="hidden" name="custom" value="{$smarty.session.USERID}">
<input type="hidden" name="amount" value="{$p.totalprice|stripslashes}">
<input type="hidden" name="currency_code" value="{$currency}">
<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="no_shipping" value="2">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="return" value="{$baseurl}/thank_you?g={$eid}">
<input type="hidden" name="cancel_return" value="{$baseurl}/">
<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
<input type="hidden" name="address_override" value="1">
<input type="hidden" name="notify_url" value="{$baseurl}/ipn_res.php">
<input type="hidden" name="produto" value="serviço">
<input type="hidden" name="id_produto" value="{$p.PID|stripslashes}">
</form>

<form action="" method="post" id="bal_form" name="bal_form">
<input type="hidden" name="subbal" value="1">
</form>                    

<form method="post" action="https://secure.payza.com/checkout" id="alertpay_form" name="alertpay_form">
<input type="hidden" name="ap_merchant" value="{$alertpay_email}"/>
<input type="hidden" name="ap_purchasetype" value="service"/>
<input type="hidden" name="ap_itemname" value="#{$p.PID|stripslashes}"/>
<input type="hidden" name="ap_amount" value="{$p.totalprice|stripslashes}"/>
<input type="hidden" name="ap_currency" value="{$alertpay_currency}"/>
<input type="hidden" name="ap_quantity" value="1"/>
<input type="hidden" name="ap_itemcode" value="{$smarty.session.USERID}"/>
<input type="hidden" name="ap_description" value="{$p.gtitle|stripslashes}"/>
<input type="hidden" name="ap_returnurl" value="{$baseurl}/thank_you?g={$eid}"/>
<input type="hidden" name="ap_cancelurl" value="{$baseurl}/"/>
<input type="hidden" name="apc_1" value="{$p.IID|stripslashes}"/>                        
</form>

<form action="" method="post" id="scriptolution_mybal_form" name="scriptolution_mybal_form">
<input type="hidden" name="scriptolution_mybal" value="1">
</form>                              
<div class="bodybg scriptolutionpaddingtop15 scriptolutionopages">
	<div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth842 gray">
		<div class="inner-wrapper scriptolutionwidth842">
			<div class="left-side scriptolutionwidth842">
				<div class="whiteBox twoHalfs padding0 scriptolutionwidth800">
                    
                    
                    <div id="scriptolutionOrderingForm" class="scriptolutionpadding20"> 
                    
                    {if $afunds LT $p.totalprice}
                        {if $enable_paypal eq "1" AND $enable_alertpay eq "1" AND $funds LT $p.totalprice}
                        <h1><strong>{$lang410}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>                             
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "1" AND $funds gte $p.totalprice}
                        <h1><strong>{$lang446}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>                             
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "0" AND $funds LT $p.totalprice}
                        
                        
                        <!--<h1><strong>{$lang259}</strong></h1> 
                        <h2>{$lang258}</h2> -->
                        <div class="row">
                            <div class="col-md-8">
                            <h1><strong>Revise seu pedido</strong></h1> 
                            <h4>Verifique se os dados estão corretos:</h4><br />
                            <p><strong>Serviço:</strong> {$p.gtitle|stripslashes}
                            {if $p.totalprice > 50}
                            <strong>(mais extras)</strong>
                            {/if}
                            <br />
                            <strong>Valor total:</strong> R$ {$p.totalprice|stripslashes}</p><p></p><br />
                            </div>
                            
                            <div class="col-md-4">
                            <br />
                            <img src="https://jetbees.com/images/formas_pagamento_500x327.jpg" class="img-responsive hidden-xs hidden-sm"/>
                            </div>
                        </div>
                        
                        <p> Ao clicar em "Fazer pagamento" você será redirecionado para o site do MercadoPago, onde deverá entrar com os dados financeiros para concluir a transação. Suas informações de pagamento serão enviadas diretamente ao MercadoPago, sem passar pelos servidores da JetBees.</p>
                        
                        <h4>O que vem a seguir</h4>
                        
                        <p>Depois que o pagamento for autorizado pelo MercadoPago, você receberá um e-mail com as instruções para prosseguir com seu pedido e informar suas necessidades ao vendedor.</p><br/>
                        
                        <link rel="stylesheet" href="https://www.jetbees.com/css/bootstrap.min.css">

<script src="https://www.jetbees.com/js/jquery-1.3.2.min.js"></script>

<script src="https://www.jetbees.com/js/bootstrap.min.js"></script>

<button type="submit" id="fazer_pagamento" class="btn btn-primary center-block">Fazer pagamento</button><p></p><br />
                        
                        
                        <div class="col-md-4"><img src="https://jetbees.com/images/formas_pagamento_500x327.jpg" class="img-responsive hidden-md hidden-lg hidden-xl" style="margin:0 auto;"/></div>
                        
                        <br />
                        
                        <div class="progress"></div>
                        
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "0" AND $funds gte $p.totalprice}
                        <h1><strong>{$lang410}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2> 
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "0" AND $enable_alertpay eq "1" AND $funds LT $p.totalprice}
                        <h1><strong>{$lang259}</strong></h1> 
                        <h2>{$lang258}</h2> 
                        <div class="progress"></div>
                        {elseif $enable_paypal eq "0" AND $enable_alertpay eq "1" AND $funds gte $p.totalprice}
                        <h1><strong>{$lang410}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2>
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {else}
                        <h1><strong>{$lang550}</strong></h1>
                        {if $enable_paypal eq "1"}<h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>{/if} 
                        {if $enable_alertpay eq "1"}<h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>{/if}
                        {if $funds gte $p.totalprice}<h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2>{/if}
                        {if $afunds gte $p.totalpricee}<h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>{/if}
                        {/if}
                    {else}
                        {if $enable_paypal eq "1" AND $enable_alertpay eq "1" AND $funds LT $p.totalprice AND $afunds gte $p.totalprice}
                        <h1><strong>{$lang446}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>                             
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "1" AND $funds gte $p.totalprice AND $afunds gte $p.totalprice}
                        <h1><strong>{$lang550}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>                             
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2> 
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "0" AND $funds LT $p.totalprice AND $afunds gte $p.totalprice}
                        <h1><strong>{$lang410}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2> 
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "1" AND $enable_alertpay eq "0" AND $funds gte $p.totalprice AND $afunds gte $p.totalprice}
                        <h1><strong>{$lang446}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2> 
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2> 
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2> 
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {elseif $enable_paypal eq "0" AND $enable_alertpay eq "1" AND $funds LT $p.totalprice AND $afunds gte $p.totalprice}
                        <h1><strong>{$lang410}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2>     
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>                            
                        {elseif $enable_paypal eq "0" AND $enable_alertpay eq "1" AND $funds gte $p.totalpric AND $afunds gte $p.totalpricee}
                        <h1><strong>{$lang446}</strong></h1>
                        <h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2>
                        <h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2>   
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {else}
                        <h1><strong>{$lang550}</strong></h1>
                        {if $enable_paypal eq "1"}<h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();">{$lang411}</a></h2>{/if}
                        {if $enable_alertpay eq "1"}<h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();">{$lang447}</a></h2>{/if}
                        {if $funds gte $p.totalprice}<h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();">{$lang412}</a></h2>{/if}
                        {if $afunds gte $p.totalpricee}<h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();">{$lang518}</a></h2>{/if}
                        <h2>{$lang489}: {if $scriptolution_cur_pos eq "1"}{$p.totalprice}{$lang197}{else}{$lang197}{$p.totalprice}{/if}</h2>
                        {/if}
                    {/if}
                    </div>
                    
                    
                    
                    
                    
                    
					<div class="clear"></div>
				</div>
			</div>			
			<div class="clear"></div>
		</div>   
	</div>
</div>
<div id="scriptolutionnobottom">
    <div class="centerwrap footertop">
        <div class="footerbg gray scriptolutionfooter842"></div>
    </div>
</div>