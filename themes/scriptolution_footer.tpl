<div class="footer">
	<div class="centerwrap footertop">
    	<div class="footerbg"></div>
    	<div class="flogo"><a href="{$baseurl}/"><img src="{$imageurl}/scriptolution_footer_logo.png" alt="scriptolution" /></a></div>
      	{include file='scriptolution_po.tpl'}
        <div class="bottomlink">
        	<ul>
            	<li><a href="{$baseurl}/terms_of_service">{$lang253}</a></li>
                <li><a href="{$baseurl}/privacy_policy">{$lang415}</a></li>
                <li><a href="{$baseurl}/contact">{$lang417}</a></li>
            </ul>
            <ul>
            	<li><a href="{$baseurl}/about">{$lang416}</a></li>
                <li><a href="{$baseurl}/advertising">{$lang418}</a></li>
                {if $enable_levels eq "1" AND $price_mode eq "3"}<li><a href="{$baseurl}/levels">{$lang500}</a></li>{/if}
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="scriptolutionfooterlang">
    <center>{include file='lang.tpl'}</center>
    </div>
</div>

<link href="{$baseurl}/css/scriptolution_countries.php" media="screen" rel="stylesheet" type="text/css" />  
<link href="{$baseurl}/css/scriptolutionresponse.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="{$baseurl}/js/jquery.customSelect.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"</script>

<script src="{$baseurl}/js/scriptolution.js"></script>
<script src="{$baseurl}/js/scriptolution_notifications.js"></script>
{include file='scriptolution_colorbox.tpl'} 
{include file='scriptolution_tooltip.tpl'} 
<script src="{$baseurl}/js/jscriptolution.js"></script>

{$smarty.capture.footer}

</body>
</html>