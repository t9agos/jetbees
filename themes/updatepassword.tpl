<!-- {include file="scriptolution_error7.tpl"} -->
{literal}

<script src="https://www.jetbees.com/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
    $('#submit').click(function(event){
    
        data = $('#newpassword').val();
        var len = data.length;
        
        if(len < 1) {
			$('#error').text("* A senha não pode estar vazia");
           // alert("A senha não pode estar vazia");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('#newpassword').val() != $('#confirm_password').val()) {
			$('#error').text("* Senha e confirmação não conferem");
            //alert("Senha e confirmação não conferem");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
});
</script>
{/literal}

<div class="bodybg scriptolutionpaddingtop15 scriptolutionloginpage">
	<div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth482">
		<div class="inner-wrapper scriptolutionwidth442">
			<div class="left-side scriptolutionwidth442">
				<div class="whiteBox twoHalfs padding15 scriptolutionwidth400">
                
                {if $updatestatus == "success"}
                	<h2 align="center">Sucesso!</h2>
                    <h3 align="center">Sua senha foi alterada. Você já pode</h3>
                    <h3 align="center"><a href="{$baseurl}/login">FAZER LOGIN</a></h3>
				{else}
                  	<h2 align="center">Erro!</h2>
                    <h3 align="center">Ocorreu alguma falha ao tentar redefinir sua senha. Por favor, <a href="{$baseurl}/forgotpassword">TENTE NOVAMENTE</a></h3>	
                {/if}
                  
                    
					<div class="clear"></div>
				</div>
			</div>			
			<div class="clear"></div>
            <div id="scriptolutionFormLinks">
                <div class="scriptolutionloginsignuplink">
                    <a href="{$baseurl}/signup{if $r ne ""}?r={$r|stripslashes}{/if}">{$lang48} {$lang49}</a>
                </div>
            </div>
		</div>   
	</div>
</div>
<div id="scriptolutionnobottom">
    <div class="centerwrap footertop">
        <div class="footerbg scriptolutionfooter482"></div>
    </div>
</div>
{if $enable_fc eq "1"}
<div class="social-wrap" align="center">
    <a href="https://www.facebook.com/dialog/permissions.request?app_id={$FACEBOOK_APP_ID}&display=page&next={$baseurl}/&response_type=code&fbconnect=1&perms=email"><span>{$lang469}</span></a>
</div>
{/if}