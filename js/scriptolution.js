jQuery(document).ready(function() {
	jQuery(".dropdown dt a").click(function() {
		jQuery(this).parent().parent().find("dd ul").toggle();
		return false;
	});
	jQuery(".dropdown dd ul li a").click(function(e) {
		var text = jQuery(this).html();
		jQuery(this).parent().parent().parent().parent().find("dt a span").html(text);
		jQuery(this).parent().parent().parent().parent().find("dd ul").hide();
		//e.preventDefault();
	});		
	function getSelectedValue(id) {
		return jQuery("#" + id).find("dt a span.value").html();
	}
	jQuery(document).bind('click', function(e) {
		var jQueryclicked = jQuery(e.target);
		if (! jQueryclicked.parents().hasClass("dropdown"))
			jQuery(".dropdown dd ul").hide();
	});		
	jQuery('.customDropdown').customSelect();
	jQuery('.order-extras').hide();
	jQuery(".hoverMe").on('mouseenter',function() {
		jQuery('.order-extras').show();
		jQuery('.order-extras').addClass('open');
	});
	jQuery(".hoverMe").on('mouseleave',function() {
		jQuery('.order-extras').hide();
		jQuery('.order-extras').removeClass('open');
	});
	jQuery(".order-extras").on('mouseenter',function() {
		jQuery('.order-extras').show();
		jQuery('.order-extras').addClass('open');
	});
	jQuery(".order-extras").on('mouseleave',function() {
		jQuery('.order-extras').hide();
		jQuery('.order-extras').removeClass('open');
	});
	jQuery('.contactUser').hide();
	jQuery(".contactPopup").on('click',function() {
		jQuery('.contactUser').fadeIn();
		jQuery('html, body').animate({
			scrollTop: jQuery(".contactUser").offset().top
		}, 800);
		return false;
	});
});
function scriptolution_newtoggle(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display != 'none' ) {
		$('#' + obj).hide();
	}
	else {
		$('#' + obj).show();
	}
}
function scriptolution_newhide(obj) {
	var el = document.getElementById(obj);
	$('#' + obj).hide();
}
function scriptolution_newshow(obj) {
	var el = document.getElementById(obj);
	$('#' + obj).show();
}