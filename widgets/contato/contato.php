<link rel="stylesheet" href="https://www.jetbees.com/css/bootstrap.min.css">

<script src="https://www.jetbees.com/js/jquery-1.3.2.min.js"></script>

<script src="https://www.jetbees.com/js/bootstrap.min.js"></script>




<?php
    if (isset($_POST["submit"])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $human = intval($_POST['human']);
        $from = "Formulário de contado JetBees"; 
        $to = 'contato@jetbees.com'; 
        $subject = 'Formulário de Contato ';
        
        $body = "De: $name\n E-Mail: $email\n Mensagem:\n $message";
 
        // Check if name has been entered
        if (!$_POST['name']) {
            $errName = 'Por favor, coloque seu nome';
        }
        
        // Check if email has been entered and is valid
        if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errEmail = 'Por favor, coloque um email válido';
        }
        
        //Check if message has been entered
        if (!$_POST['message']) {
            $errMessage = 'Por favor, digite sua mensagem';
        }
        //Check if simple anti-bot test is correct
        if ($human !== 8) {
            $errHuman = 'A verificação anti-spam está incorreta';
        }
 
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
    if (mail ($to, $subject, $body, $from)) {
        $result='<div class="alert alert-success">Sua mensagem foi enviada com sucesso. Entraremos em contato o mais breve possível</div>';
    } else {
        $result='<div class="alert alert-danger">Desculpe, ocorreu um erro ao enviar a mensagem. Tente novamente mais tarde</div>';
    }
}
    }
	
?>


<form class="form-horizontal" role="form" method="post" action="https://www.jetbees.com/contact">

	<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php echo $result; ?>    
        </div>
    </div>


    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nome</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Nome completo" value="<?php echo htmlspecialchars($_POST['name']); ?>">
            <?php echo "<p class='text-danger'>$errName</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="examplo@dominio.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
            <?php echo "<p class='text-danger'>$errEmail</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Mensagem</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
            <?php echo "<p class='text-danger'>$errMessage</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="human" class="col-sm-2 control-label">2 + 6 = ?</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="human" name="human" placeholder="Sua resposta">
            <?php echo "<p class='text-danger'>$errHuman</p>";?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Enviar" class="btn btn-primary">
        </div>
    </div>
    </form> 

