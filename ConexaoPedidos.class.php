<?php
class ConexaoPedidos extends PDO
{
	public function __construct()
	{
		//Carrega arquivo de conexão
		$config = parse_ini_file('configs/pedidos.ini', true);
		
		try {
			/*
			 * Abre uma conexão com banco de dados
			 * Executa o construtor da classe "pai", que é a classe PDO
			 */
			
			parent::__construct("{$config['adapter']}:host={$config['host']};dbname={$config['dbname']}", $config['username'], $config['password'], array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			));
			PDO::exec("SET NAMES '{$config['charset']}';");
			
		} catch (Exception $e) {
			echo "Erro ao conectar ao banco de dados: " . $e->getMessage();
		}
	}
}