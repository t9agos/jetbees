<?php
/**************************************************************************************************
| Fiverr Script
| http://www.fiverrscript.com
| webmaster@fiverrscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.fiverrscript.com/eula.html and to be bound by it.
|
| Copyright (c) FiverrScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

require("include/password.php");

if(isset($_POST['r'])){

$rcode = $_POST['r'];
$rcode = base64_decode($rcode);

$rcode = explode("jFb5s752", $rcode);

$id_request = $rcode[0];
$hash_request = $rcode[1];

$query="SELECT id, date, email, user_id, hash, addtime FROM jb_forgotpassword WHERE id='".mysql_real_escape_string($id_request)."'";
		$result=$conn->execute($query);
		$id = $result->fields['id'];
		$date = $result->fields['date'];
		$email = $result->fields['email'];
		$user_id = $result->fields['user_id'];
		$hash = $result->fields['hash'];
		$addtime = $result->fields['addtime'];
		
if($id == $id_request && $hash == $hash_request){
	
	$query="SELECT USERID, email, addtime FROM members WHERE USERID='".mysql_real_escape_string($user_id)."' AND email='".mysql_real_escape_string($email)."'";
		$result=$conn->execute($query);
		$USERID= $result->fields['USERID'];
		$user_email= $result->fields['email'];
		$user_addtime= $result->fields['addtime'];
		
		if($user_id == $USERID && $user_email == $email && $addtime == $user_addtime){
			
			$newpwd = $_POST["newpassword"];
			
			$newpwd = password_hash($newpwd, PASSWORD_BCRYPT);
			
			$query="UPDATE members SET password='".mysql_real_escape_string($newpwd)."' WHERE USERID='".mysql_real_escape_string($USERID)."' AND email='".mysql_real_escape_string($user_email)."'";
			$result=$conn->execute($query);
			
			$query="DELETE FROM jb_forgotpassword WHERE id='".mysql_real_escape_string($id)."'";
			$result=$conn->execute($query);
	
			
			$updatestatus = "success";	
		}else{
			$updatestatus = "error";	
		}
		
	
		
}

}

$templateselect = "updatepassword.tpl";
$pagetitle = "Redefinir senha";
STemplate::assign('pagetitle',$pagetitle);
STemplate::assign("updatestatus",$updatestatus);

//TEMPLATES BEGIN
STemplate::display('scriptolution_header.tpl');
STemplate::display($templateselect);
STemplate::display('scriptolution_footer_nobottom.tpl');

?>