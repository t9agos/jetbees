<?php

require_once "mercadopago.php";

//////////////////////////////////////////////////////////////////////

//$id_vendedor = $_GET["id"];
if($_GET["produto"] == "featured"){
		//Usuário teste $mp = new MP("3456456602783189", "cA3z60Mh4oY0jymyZsWrkmeZAR2Q3xRH");
	$mp = new MP("5660367234250529", "UDRnBhTPw6Y2qdWJNVnoIeAbI6SS6Vzn");
}else{
	require 'ConexaoPedidos.class.php';
	
	//Cria o objeto da conexão
	$pdo = new ConexaoPedidos();
	
	//Monta a string da query (consulta) SQL
	$query = "SELECT USERID, addtime, token FROM members WHERE USERID = :id_vendedor";
		
	$stmt = $pdo->prepare($query);
	
	$stmt->bindParam(":id_vendedor", $_GET["id_vendedor"]);
	
	//Executar a consulta no banco
	$stmt->execute();
	
	//Retorna os valores em array
	$resultado = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$token_vendedor = $resultado['token'];
	$id = $resultado['USERID'];
	$addtime = $resultado['addtime'];
	
	define('ENCRYPTION_KEY', hash('sha256','.9d81*jYZ'.$addtime.'S.h13"847HDIM9E:"0d|Wo'.$id.'3B61YWu50rP7'.$id.$addtime.'7O7l#bia#6Ui8k'.$addtime.'Jp1X2K3h80oGcS71[w'.$addtime.'<]v8<048.@*|k0'.$id.'"/9U16!6'));
	
	function mc_decrypt($decrypt, $key){
		$decrypt = explode('|', $decrypt.'|');
		$decoded = base64_decode($decrypt[0]);
		$iv = base64_decode($decrypt[1]);
		if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
		$key = pack('H*', $key);
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
		$mac = substr($decrypted, -64);
		$decrypted = substr($decrypted, 0, -64);
		$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
		if($calcmac!==$mac){ return false; }
		$decrypted = unserialize($decrypted);
		return $decrypted;
	}
	
	
	$mp = new MP(mc_decrypt($token_vendedor, ENCRYPTION_KEY));

}

$topic = $_GET["topic"];
$merchant_order_info = null;

switch ($topic) {
    case 'payment':
        $payment_info = $mp->get("/collections/notifications/".$_GET["id"]);
        $merchant_order_info = $mp->get("/merchant_orders/".$payment_info["response"]["collection"]["merchant_order_id"]);
        break;
    case 'merchant_order':
        $merchant_order_info = $mp->get("/merchant_orders/".$_GET["id"]);
		        break;
    default:
        $merchant_order_info = null;
}

if($merchant_order_info == null) {
    echo "Error obtaining the merchant_order";
    die();
}



if ($merchant_order_info["status"] == 200) {

/*
pending - O usuário ainda não completou o processo de pagamento.
approved - O pagamento foi aprovado e acreditado.
in_process - O pagamento estão em revisão.
in_mediation - Os usuários tem começada uma disputa.
rejected - O pagamento foi rejeitado. O usuário pode tentar novamente.
cancelled - O pagamento foi cancelado por uma das parte ou porque o tempo expirou.
refunded - O pagamento foi devolvido ao usuário.
charged_back - Foi feito um chargeback no cartão do comprador.
*/

$status = "";//Declaração da variável que vai receber o equivalente do status do paypal

$status_pagamento = $merchant_order_info["response"]["payments"][0]["status"];

switch ($status_pagamento) {
    case "approved":
        $status_pagamento = "Completed";
        break;
    case "pending":
        $status_pagamento = "Pending";
        break;
	case "in_process":
        $status_pagamento = "Pending";
        break;
    case "rejected":
        $status_pagamento = "Denied";
        break;
    case "cancelled":
        $status_pagamento = "Failed";
        break;
    case "in_mediation": //comprador abriu uma disputa
        $status_pagamento = "Pending";
        break;
	case "refunded":
        $status_pagamento = "Refunded";
        break;
	case "charged_back":
        $status_pagamento = "Reversed";
        break;
}

$id_transcao = $merchant_order_info["response"]["id"];
$email_vendedor = $merchant_order_info["response"]["collector"]["email"];
$id_vendedor = $merchant_order_info["response"]["collector"]["id"];
$id_item = $merchant_order_info["response"]["items"][0]["id"];
$nome_item = $merchant_order_info["response"]["items"][0]["title"];
$id_pagamento = $merchant_order_info["response"]["payments"][0]["id"];
$tipo_pagamento = $merchant_order_info["response"]["payments"][0]["operation_type"];
$valor = $merchant_order_info["response"]["payments"][0]["transaction_amount"];
$data_pagamento = $merchant_order_info["response"]["payments"][0]["date_approved"];
$moeda = $merchant_order_info["response"]["payments"][0]["currency_id"];
$id_comprador = $merchant_order_info["response"]["payer"]["id"];
$email_comprador = $merchant_order_info["response"]["payer"]["email"];
$nome_comprador = $merchant_order_info["response"]["payer"]["nickname"];
$user_id = $merchant_order_info["response"]["external_reference"];
//$id_produto = $merchant_order_info["response"]["additional_info"];
$produto = $merchant_order_info["response"]["additional_info"];



if($produto == "featured"){							
	$url = 'https://www.jetbees.com/ipn_pf.php';
}else{
	$url = 'https://www.jetbees.com/ipn_res.php';
}

$data = array('receiver_email' => $email_vendedor, //$recebedor_login,
		'receiver_id' => $id_vendedor,
		'residence_country' => $pagador_pais,
		'test_ipn' => '0',
		'transaction_subject' => $nome_item,
		'txn_id' => $id_transcao,
		'txn_type' => $tipo_pagamento,
		'payer_email' => $email_comprador,
		'payer_id' => $id_comprador,
		'payer_status' => '',
		'first_name' => $nome_comprador,
		'last_name' => '',
		'address_city' => '',
		'address_country' => '',
		'address_state' => '',
		'address_status' => '',
		'address_country_code' => '',
		'address_name' => '',
		'address_street' => '',
		'address_zip' => '',
		'custom' => $user_id,//user id ******************** ATENÇÃO ***************
		'handling_amount' => $valor,
		'item_name' => $nome_item,
		'item_number' => $id_item,
		'mc_currency' => $moeda,
		'mc_fee' => '',
		'mc_gross' => $valor,
		'payment_date' => $data_pagamento,
		'payment_fee' => '',
		'payment_gross' => $valor,
		'payment_status' => $status_pagamento,
		'payment_type' => $tipo_pagamento,
		'protection_eligibility' => '',
		'quantity' => '1',
		'shipping' => '',
		'tax' => '',
		'notify_version' => '',
		'charset' => '',
		'verify_sign' => '',
		);

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ),
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);


}//Fim do if 200
?>