<?php /* Smarty version Smarty-3.0.7, created on 2015-12-06 19:10:21
         compiled from "/home/marcosta/public_html/themes/resetpassword.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10522240025664a43dd2d1a2-23123875%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c6f1d69801399b42b2bf00b0201b65314ec0640' => 
    array (
      0 => '/home/marcosta/public_html/themes/resetpassword.tpl',
      1 => 1449435987,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10522240025664a43dd2d1a2-23123875',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- <?php $_template = new Smarty_Internal_Template("scriptolution_error7.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> -->


<script src="https://www.jetbees.com/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
    $('#submit').click(function(event){
    
        data = $('#newpassword').val();
        var len = data.length;
        
        if(len < 1) {
			$('#error').text("* A senha não pode estar vazia");
           // alert("A senha não pode estar vazia");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('#newpassword').val() != $('#confirm_password').val()) {
			$('#error').text("* Senha e confirmação não conferem");
            //alert("Senha e confirmação não conferem");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
});
</script>


<div class="bodybg scriptolutionpaddingtop15 scriptolutionloginpage">
	<div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth482">
		<div class="inner-wrapper scriptolutionwidth442">
			<div class="left-side scriptolutionwidth442">
				<div class="whiteBox twoHalfs padding15 scriptolutionwidth400">
                
                <?php if ($_smarty_tpl->getVariable('linkstatus')->value=="valid"){?>
					<h1>Redefinir senha</h1>
                    <div id="scriptolutionForm">
                        <form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/updatepassword.php" id="resetpassword" method="post">  
                        <div id="error" style="color: red;"></div>
                            <div class="form-entry">
                            	<div class="form-label">
                                <label for="newpassword">Digite sua nova senha</label>
                                </div>
                                <input class="text" id="newpassword" name="newpassword" tabindex="1" type="password"  />
                            </div>
                            <div class="form-entry">
                                <div class="form-label">
                                    <label for="confirm_password">Redigite sua senha</label>
                                </div>
                                <input class="text" id="confirm_password" name="confirm_password" tabindex="2" type="password" />
                                <input type="hidden" name="r" value="<?php echo $_smarty_tpl->getVariable('r')->value;?>
" />
                            </div>
                            <div class="row">
                                <input type="submit" value="Enviar" class="scriptolutionbluebutton" id="submit" />
                            </div>
                        </form>   
                    </div>
                  <?php }else{ ?>
                  		<h2 align="center">Link inválido!</h2>
                        <br />
                        <h3 align="center">Para resetar sua senha, <a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/forgotpassword">CLIQUE AQUI</a> e faça uma nova solicitação</h3>
                   <?php }?>
                  
                    
					<div class="clear"></div>
				</div>
			</div>			
			<div class="clear"></div>
            <div id="scriptolutionFormLinks">
                <div class="scriptolutionloginsignuplink">
                    <a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/signup<?php if ($_smarty_tpl->getVariable('r')->value!=''){?>?r=<?php echo stripslashes($_smarty_tpl->getVariable('r')->value);?>
<?php }?>"><?php echo $_smarty_tpl->getVariable('lang48')->value;?>
 <?php echo $_smarty_tpl->getVariable('lang49')->value;?>
</a>
                </div>
            </div>
		</div>   
	</div>
</div>
<div id="scriptolutionnobottom">
    <div class="centerwrap footertop">
        <div class="footerbg scriptolutionfooter482"></div>
    </div>
</div>
<?php if ($_smarty_tpl->getVariable('enable_fc')->value=="1"){?>
<div class="social-wrap" align="center">
    <a href="https://www.facebook.com/dialog/permissions.request?app_id=<?php echo $_smarty_tpl->getVariable('FACEBOOK_APP_ID')->value;?>
&display=page&next=<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/&response_type=code&fbconnect=1&perms=email"><span><?php echo $_smarty_tpl->getVariable('lang469')->value;?>
</span></a>
</div>
<?php }?>