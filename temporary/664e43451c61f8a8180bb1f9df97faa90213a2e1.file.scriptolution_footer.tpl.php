<?php /* Smarty version Smarty-3.0.7, created on 2016-12-03 21:52:43
         compiled from "/home/marcosta/public_html/themes/scriptolution_footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1600288658435acbcc1209-79879917%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '664e43451c61f8a8180bb1f9df97faa90213a2e1' => 
    array (
      0 => '/home/marcosta/public_html/themes/scriptolution_footer.tpl',
      1 => 1480809157,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1600288658435acbcc1209-79879917',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="footer">
	<div class="centerwrap footertop">
    	<div class="footerbg"></div>
    	<div class="flogo"><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/"><img src="<?php echo $_smarty_tpl->getVariable('imageurl')->value;?>
/scriptolution_footer_logo.png" alt="scriptolution" /></a></div>
      	<?php $_template = new Smarty_Internal_Template('scriptolution_po.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <div class="bottomlink">
        	<ul>
            	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/terms_of_service"><?php echo $_smarty_tpl->getVariable('lang253')->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/privacy_policy"><?php echo $_smarty_tpl->getVariable('lang415')->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/contact"><?php echo $_smarty_tpl->getVariable('lang417')->value;?>
</a></li>
            </ul>
            <ul>
            	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/about"><?php echo $_smarty_tpl->getVariable('lang416')->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/advertising"><?php echo $_smarty_tpl->getVariable('lang418')->value;?>
</a></li>
                <?php if ($_smarty_tpl->getVariable('enable_levels')->value=="1"&&$_smarty_tpl->getVariable('price_mode')->value=="3"){?><li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/levels"><?php echo $_smarty_tpl->getVariable('lang500')->value;?>
</a></li><?php }?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="scriptolutionfooterlang">
    <center><?php $_template = new Smarty_Internal_Template('lang.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?></center>
    </div>
</div>

<link href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/css/scriptolution_countries.php" media="screen" rel="stylesheet" type="text/css" />  
<link href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/css/scriptolutionresponse.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.customSelect.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"</script>

<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/scriptolution.js"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/scriptolution_notifications.js"></script>
<?php $_template = new Smarty_Internal_Template('scriptolution_colorbox.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> 
<?php $_template = new Smarty_Internal_Template('scriptolution_tooltip.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jscriptolution.js"></script>

<?php echo Smarty::$_smarty_vars['capture']['footer'];?>


</body>
</html>