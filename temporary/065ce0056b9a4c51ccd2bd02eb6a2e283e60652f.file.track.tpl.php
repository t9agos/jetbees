<?php /* Smarty version Smarty-3.0.7, created on 2015-10-08 09:07:41
         compiled from "/home/marcosta/public_html/themes/track.tpl" */ ?>
<?php /*%%SmartyHeaderCode:28069326656166a9dc58a62-45283519%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '065ce0056b9a4c51ccd2bd02eb6a2e283e60652f' => 
    array (
      0 => '/home/marcosta/public_html/themes/track.tpl',
      1 => 1443800331,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '28069326656166a9dc58a62-45283519',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include '/home/marcosta/public_html/smarty/libs/plugins/modifier.date_format.php';
?><script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.uploadify.v2.1.0.min.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/swfobject.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.scrollTo-min.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.corner.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.qtip-1.0.0-rc3.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.hint.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/orders.php" type="text/javascript"></script>
<link href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/css/scriptolutionorders.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<?php $_template = new Smarty_Internal_Template("scriptolution_error7.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>            
<div id="action-bar" class="mp-box action-bar-orders scriptolutiontrackingpage">
	<div class="box-row">
		<div class="action-steps cf">
        	<div class="step-end">
                <h5>
                <b>
                <?php if ($_smarty_tpl->getVariable('o')->value['status']=="0"){?>
                <?php echo $_smarty_tpl->getVariable('lang278')->value;?>

                <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="1"){?>
                <?php echo $_smarty_tpl->getVariable('lang279')->value;?>

                <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="2"||$_smarty_tpl->getVariable('o')->value['status']=="3"||$_smarty_tpl->getVariable('o')->value['status']=="7"){?> 
                <?php echo $_smarty_tpl->getVariable('lang203')->value;?>

                <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="4"){?>
                <?php echo $_smarty_tpl->getVariable('lang201')->value;?>

                <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="5"){?>
                <?php echo $_smarty_tpl->getVariable('lang202')->value;?>

                <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="6"){?>
                <?php echo $_smarty_tpl->getVariable('lang320')->value;?>

                <?php }?>
                </b>
                </h5>
            </div>
                                
			<div class="step">

                <div class="order-meta"> 
                    <?php if ($_smarty_tpl->getVariable('o')->value['status']=="0"){?>
                    <h3 class="flag"><span class="status waiting-for-reqs" title="<?php echo $_smarty_tpl->getVariable('lang278')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang278')->value;?>
</span></h3> 
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="1"){?>
                    <h3 class="flag"><span class="status in-progress" title="<?php echo $_smarty_tpl->getVariable('lang279')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang279')->value;?>
</span></h3>
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="2"||$_smarty_tpl->getVariable('o')->value['status']=="3"||$_smarty_tpl->getVariable('o')->value['status']=="7"){?> 
                    <h3 class="flag"><span class="status order-cancelled" title="<?php echo $_smarty_tpl->getVariable('lang203')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang203')->value;?>
</span></h3>
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="4"){?>
                    <h3 class="flag"><span class="status order-delivered" title="<?php echo $_smarty_tpl->getVariable('lang201')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang201')->value;?>
</span></h3>
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="5"){?>
                    <h3 class="flag"><span class="status order-completed" title="<?php echo $_smarty_tpl->getVariable('lang202')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang202')->value;?>
</span></h3>
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="6"){?>
                    <h3 class="flag"><span class="status order-rejected" title="<?php echo $_smarty_tpl->getVariable('lang321')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang140')->value;?>
 #<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
: <?php echo $_smarty_tpl->getVariable('lang320')->value;?>
</span></h3>
                    <?php }?>
                </div>
            
			</div>
		</div>
	</div>
</div>            
<div class="bodybg scriptolutionpaddingtop15">
	<div class="whitebody scriptolutionpaddingtop30 gray">
		<div class="inner-wrapper">
			<div class="clear"></div>
			<div class="left-side">
				<div class="whiteBox twoHalfs padding15 scriptolutionshop">
                    <div class="order-image" style="float:left"> 
                        <img src="<?php echo $_smarty_tpl->getVariable('purl')->value;?>
/t2/<?php echo $_smarty_tpl->getVariable('o')->value['p1'];?>
?<?php echo time();?>
" /> 
                    </div>
                    <div style="float:left; padding-left:15px;">    
						<h1><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo insert_get_seo_profile(array('value' => 'a', 'username' => stripslashes($_smarty_tpl->getVariable('o')->value['username'])),$_smarty_tpl);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('o')->value['username']);?>
</a> <?php echo $_smarty_tpl->getVariable('lang262')->value;?>
 <?php $_smarty_tpl->assign('title' , insert_seo_clean_titles (array('value' => 'a', 'title' => $_smarty_tpl->getVariable('o')->value['gtitle']),$_smarty_tpl), true);?><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo stripslashes($_smarty_tpl->getVariable('o')->value['seo']);?>
/<?php echo stripslashes($_smarty_tpl->getVariable('o')->value['PID']);?>
/<?php echo $_smarty_tpl->getVariable('title')->value;?>
"><?php echo stripslashes($_smarty_tpl->getVariable('o')->value['gtitle']);?>
</a> <?php if ($_smarty_tpl->getVariable('scriptolution_cur_pos')->value=="1"){?><?php echo $_smarty_tpl->getVariable('lang589')->value;?>
 <?php echo stripslashes($_smarty_tpl->getVariable('o')->value['price']);?>
<?php echo $_smarty_tpl->getVariable('lang197')->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('lang63')->value;?>
<?php echo stripslashes($_smarty_tpl->getVariable('o')->value['price']);?>
<?php }?></h1> 
                    </div>
                    <div style="clear:both; padding-bottom:5px;"></div>
                    <h4>
                    	<a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php $_smarty_tpl->assign('cvseo' , insert_get_seo_convo (array('value' => 'a', 'username' => stripslashes($_smarty_tpl->getVariable('o')->value['username'])),$_smarty_tpl), true);?>"><?php echo $_smarty_tpl->getVariable('lang235')->value;?>
 <?php echo stripslashes($_smarty_tpl->getVariable('o')->value['username']);?>
</a>
                    	
                    </h4>
                    <div style="clear:both;"></div>
                    <div class="whiteBox inside" align="center">
						<?php echo $_smarty_tpl->getVariable('lang263')->value;?>
 <a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo insert_get_seo_profile(array('value' => 'a', 'username' => stripslashes($_smarty_tpl->getVariable('o')->value['username'])),$_smarty_tpl);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('o')->value['username']);?>
</a> on <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('o')->value['time_added'],"%A, %B %e %Y");?>
 <?php echo $_smarty_tpl->getVariable('lang265')->value;?>
 <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('o')->value['time_added'],"%I:%M %p");?>

					</div>
					<div class="db-main-table okay">
						<table>
                        	<thead>
								<tr>
									<td>
                                    	<?php echo $_smarty_tpl->getVariable('lang292')->value;?>

                                    </td>
								</tr>
							</thead>
							<tbody>
                            <tr>
                                <td>  
                                    <i style="color:#51DD86" class="fa fa-check-square-o fa-2x"></i>                              
                                    <div class="milestone okay" title="<?php echo $_smarty_tpl->getVariable('lang292')->value;?>
"> 
                                      <div class="status-label"></div> 
                                      <div class="mutual-status">
                                        <span><?php echo $_smarty_tpl->getVariable('lang266')->value;?>
</span> 
                                      </div> 
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="db-main-table reqbox">
						<table>
                        	<thead>
								<tr>
									<td>
                                    	<?php echo $_smarty_tpl->getVariable('lang293')->value;?>

                                    </td>
								</tr>
							</thead>
							<tbody>
                            <tr>
                                <td>                                
                                    <div class="milestone action reqbox" title="<?php echo $_smarty_tpl->getVariable('lang293')->value;?>
"> 
                                        <div class="status-label"></div> 
                                        <div class="mutual-status underway">
                                            <h3><?php echo stripslashes($_smarty_tpl->getVariable('o')->value['username']);?>
 <?php echo $_smarty_tpl->getVariable('lang270')->value;?>
</h3>
                                            <?php $_template = new Smarty_Internal_Template('track_ship.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                                            <span><p><?php echo stripslashes($_smarty_tpl->getVariable('o')->value['ginst']);?>
</p></span>
                                            <?php $_template = new Smarty_Internal_Template('track_extras.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                                        </div> 
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="msgresults">
                        <?php $_smarty_tpl->assign('lasdel' , insert_last_delivery (array('value' => 'a', 'oid' => $_smarty_tpl->getVariable('o')->value['OID']),$_smarty_tpl), true);?>
                        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('m')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        <?php $_smarty_tpl->assign('profilepicture' , insert_get_member_profilepicture (array('value' => 'var', 'USERID' => $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MSGFROM']),$_smarty_tpl), true);?>
                        <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['action']=="mutual_cancellation_request"){?>
                            <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MSGFROM']==$_SESSION['USERID']){?>                            
                            <div class="db-main-table warning" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                <table>
                                	<thead>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo $_smarty_tpl->getVariable('lang286')->value;?>

                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="leftitscriptolution width25">
                                            <i style="color:#FBC137" class="fa fa-exclamation-circle fa-3x"></i> 
                                        </td>
                                        <td class="leftitscriptolution width75">                                
                                            <div class="milestone action reqbox" title="<?php echo $_smarty_tpl->getVariable('lang293')->value;?>
"> 
                                                <div class="status-label"></div> 
                                                <div class="mutual-status underway">
                                                    <div class="padding5"></div>
                                                    <p><?php echo $_smarty_tpl->getVariable('lang287')->value;?>
: <?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</p>
                                                    <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cancel']=="0"){?>
                                                    <form name="abort<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
" method="post">
                                                    <input type="hidden" name="subabort" value="1">
                                                    <input type="hidden" name="AMID" value="<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                                    </form>
                                                    <div class="status-control"><?php echo $_smarty_tpl->getVariable('lang289')->value;?>

                                                    <div style="clear:both; padding-bottom:10px;"></div>
                                                    <a class="ascriptolutionredbutton" style="color:#fff" href="#" onclick="document.abort<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
.submit()"><?php echo $_smarty_tpl->getVariable('lang290')->value;?>
</a>
                                                    <div style="clear:both; padding-bottom:10px;"></div><?php echo $_smarty_tpl->getVariable('lang291')->value;?>
</div>
                                                    <?php }?>                                     
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php }else{ ?>  
                            <div class="db-main-table warning" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                <table>
                                	<thead>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo $_smarty_tpl->getVariable('lang288')->value;?>

                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="leftitscriptolution width25">
                                            <i style="color:#FBC137" class="fa fa-exclamation-circle fa-3x"></i> 
                                        </td>
                                        <td class="leftitscriptolution width75">                                
                                            <div class="milestone action reqbox" title="<?php echo $_smarty_tpl->getVariable('lang293')->value;?>
"> 
                                                <div class="status-label"></div> 
                                                <div class="mutual-status underway">
                                                    <div class="padding5"></div>
                                                    <p><?php echo $_smarty_tpl->getVariable('lang287')->value;?>
: <?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</p>
                                                    <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cancel']=="0"){?>
                                                    <form name="decline<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
" method="post">
                                                    <input type="hidden" name="subdecline" value="1">
                                                    <input type="hidden" name="DMID" value="<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                                    </form>
                                                    <form name="accept<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
" method="post">
                                                    <input type="hidden" name="subaccept" value="1">
                                                    <input type="hidden" name="AMID" value="<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                                    </form>
                                                    <div class="status-control"><?php echo $_smarty_tpl->getVariable('lang291')->value;?>

                                                    <div style="clear:both; padding-bottom:10px;"></div>
                                                    <a class="ascriptolutionredbutton" style="color:#fff" href="#" onclick="document.decline<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
.submit()"><?php echo $_smarty_tpl->getVariable('lang296')->value;?>
</a> - <a class="ascriptolutiongreenbutton" style="color:#fff" href="#" onclick="document.accept<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
.submit()"><?php echo $_smarty_tpl->getVariable('lang301')->value;?>
</a></div>
                                                    <?php }?>                                        
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php }?>
                            <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cancel']=="1"){?>
                            <div class="db-main-table yellowbg" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="leftitscriptolution width25">
                                            <i style="color:#FBC137" class="fa fa-times fa-3x"></i> 
                                        </td>
                                        <td class="leftitscriptolution width75">                                
                                            <div class="milestone warning" title="<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['ctime']);?>
">
                                              <div class="mutual-status duedate"><span><span><?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['CID']==$_SESSION['USERID']){?><?php echo $_smarty_tpl->getVariable('lang297')->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('lang298')->value;?>
<?php }?></span></span></div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php }elseif($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cancel']=="2"){?>
                            <div class="db-main-table redbg" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="leftitscriptolution width25">
                                            <i style="color:#FB3737" class="fa fa-times fa-3x"></i> 
                                        </td>
                                        <td class="leftitscriptolution width75">                                
                                            <div class="milestone warning" title="<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['ctime']);?>
">
                                              <div class="mutual-status duedate"><span><span><?php echo $_smarty_tpl->getVariable('lang303')->value;?>
</span></span></div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php }?>
                        <?php }elseif($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['action']=="seller_cancellation"){?>
                        <div class="db-main-table redbg" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="leftitscriptolution width25">
                                        <i style="color:#FB3737" class="fa fa-times fa-3x"></i> 
                                    </td>
                                    <td class="leftitscriptolution width75">                                
                                        <div class="milestone cancel" title="<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['ctime']);?>
">
                                          <div class="status-label"></div>
                                          <div class="mutual-status duedate"><span><?php echo $_smarty_tpl->getVariable('lang304')->value;?>
<br /><?php echo $_smarty_tpl->getVariable('lang287')->value;?>
: <?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</span></div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }elseif($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['action']=="rejection"){?>
                        <div class="db-main-table redbg">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="leftitscriptolution width25">
                                        <i style="color:#FB3737" class="fa fa-exclamation-triangle fa-3x"></i> 
                                    </td>
                                    <td class="leftitscriptolution width75">                                
                                        <div class="milestone cancel" title="<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['time']);?>
"> 
                                          <div class="status-label"></div> 
                                          <div class="mutual-status duedate"><span><?php echo $_smarty_tpl->getVariable('lang321')->value;?>
:<br /><?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</span></div> 
                                        </div> 
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }elseif($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['action']=="delivery"){?>
                        <div class="db-main-table deliverybox" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="leftitscriptolution width25">
										<i style="color:#0ABA44" class="fa fa-check fa-4x"></i> 
                                    </td>
                                    <td class="leftitscriptolution width75">                                
                                        <div class="milestone action reqbox"> 
                                            <div class="status-label"></div> 
                                            <div class="mutual-status underway">
                                                <h3 style="font-size:36px"><?php echo $_smarty_tpl->getVariable('lang308')->value;?>
</h3>
                                                <div class="padding5"></div>
                                                <span><p><?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</p></span>
                                                <?php $_template = new Smarty_Internal_Template("track_files.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
$_template->assign('scriptolutionfileinfo',$_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]); echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                                            </div> 
                                            <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID']==$_smarty_tpl->getVariable('lasdel')->value&&$_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['reject']=="0"){?>
                                            <div class="delivery-footer" style="clear:both;"> 
                                            	<div class="padding5"></div
                                                ><small>[<?php if ($_smarty_tpl->getVariable('o')->value['status']=="5"){?><?php echo $_smarty_tpl->getVariable('lang319')->value;?>
 <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('o')->value['cltime']);?>
<?php }elseif($_smarty_tpl->getVariable('o')->value['status']!="2"&&$_smarty_tpl->getVariable('o')->value['status']!="7"&&$_smarty_tpl->getVariable('o')->value['status']!="3"){?><?php echo $_smarty_tpl->getVariable('lang307')->value;?>
<?php }?>]</small>                                               
                                            </div> 
                                            <?php }?>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }else{ ?>
                            <div class="db-main-table reqbox" id="message_<?php echo $_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['MID'];?>
">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="leftitscriptolution width25">
                                            <a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo insert_get_seo_profile(array('value' => 'a', 'username' => stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['mfrom'])),$_smarty_tpl);?>
" title="<?php echo stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['mfrom']);?>
"><img alt="<?php echo stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['mfrom']);?>
" src="<?php echo $_smarty_tpl->getVariable('membersprofilepicurl')->value;?>
/thumbs/<?php echo $_smarty_tpl->getVariable('profilepicture')->value;?>
?<?php echo time();?>
" class="scriptolutionuimage" /></a>
                                        </td>
                                        <td class="leftitscriptolution width75">                                
                                            <div class="milestone action reqbox" title="<?php echo $_smarty_tpl->getVariable('lang293')->value;?>
"> 
                                                <div class="status-label"></div> 
                                                <div class="mutual-status underway">
                                                    <h3><?php echo stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['mfrom']);?>
</h3>
                                                    <div class="padding5"></div>
                                                    <span><p><?php echo nl2br(stripslashes($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['message']));?>
</p></span>
                                                    <?php $_template = new Smarty_Internal_Template("track_files.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
$_template->assign('scriptolutionfileinfo',$_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]); echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ($_smarty_tpl->getVariable('m')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['start']=="1"){?>
                            <div class="db-main-table started">
                                <table>
                                    <thead>
                                        <tr>
                                            <td>
                                                <?php echo $_smarty_tpl->getVariable('lang277')->value;?>

                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>                       
                                        	<i style="color:#00668C" class="fa fa-fighter-jet fa-3x"></i>          
                                            <div class="milestone okay" title="<?php echo $_smarty_tpl->getVariable('lang277')->value;?>
"> 
                                              <div class="status-label"></div> 
                                              <div class="mutual-status underway"><h3><?php echo $_smarty_tpl->getVariable('lang275')->value;?>
. <?php echo $_smarty_tpl->getVariable('lang277')->value;?>
</h3> 
                                                    <span>                                            
                                                        <span><?php echo $_smarty_tpl->getVariable('lang276')->value;?>
 <b><?php $_smarty_tpl->assign('deadline' , insert_get_deadline (array('value' => 'a', 'days' => $_smarty_tpl->getVariable('o')->value['days'], 'time' => $_smarty_tpl->getVariable('o')->value['stime']),$_smarty_tpl), true);?><?php echo $_smarty_tpl->getVariable('deadline')->value;?>
</b></span>                                  
                                                    </span> 
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php }?>  
                        <?php }?>
                        <?php endfor; endif; ?>
                        <?php $_smarty_tpl->assign('fbvl' , insert_fback (array('value' => 'a', 'oid' => $_smarty_tpl->getVariable('o')->value['OID']),$_smarty_tpl), true);?>
                        <?php if ($_smarty_tpl->getVariable('lasdel')->value>"0"&&$_smarty_tpl->getVariable('fbvl')->value=="0"){?>
                        <?php if ($_smarty_tpl->getVariable('o')->value['status']!="6"&&$_smarty_tpl->getVariable('o')->value['status']!="2"&&$_smarty_tpl->getVariable('o')->value['status']!="7"&&$_smarty_tpl->getVariable('o')->value['status']!="3"){?>
                        <form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/track?id=<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
" class="review_form" id="new_rating" method="post">
                        <div class="db-main-table">
                            <table>
                                <tbody>
                                <tr>
                                    <td>   
                                    	<h3 style="font-size:30px"><?php echo $_smarty_tpl->getVariable('lang310')->value;?>
</h3>                             
                                          <div class="post-order-rating"> 
                                            <input checked="checked" class="good-review-button" id="rating_value_1" name="ratingvalue" type="radio" value="1" /><i style="color:#0ABA44" class="fa fa-thumbs-up fa-2x"></i>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input class="bad-review-button" id="rating_value_0" name="ratingvalue" type="radio" value="0" /><i style="color:#F99F2A" class="fa fa-thumbs-down fa-2x"></i> 
                                          </div> 
                                          <br clear="both"/> 
                                          <div class="share-experience"> 
                                            <textarea cols="35" id="rating_comment" maxlength="300" name="ratingcomment" rows="5" title="<?php echo $_smarty_tpl->getVariable('lang311')->value;?>
" placeholder="<?php echo $_smarty_tpl->getVariable('lang311')->value;?>
"></textarea> 
                                            <br clear="all"/> 
                                          </div> 
                                          <div style="clear:both; padding-bottom:10px;"></div>
                                            <input type="submit" value="<?php echo $_smarty_tpl->getVariable('lang46')->value;?>
" class="scriptolutionbluebutton" style="color:#FFF;display: inline;"  />
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="subrat" value="1" /> 
                        </form>
                        <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->getVariable('fbvl')->value=="1"){?>
                        <div class="db-main-table yellowbg">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="leftitscriptolution width25">
                                        <i style="color:#FBC137" class="fa fa-lightbulb-o fa-3x"></i> 
                                    </td>
                                    <td class="leftitscriptolution width75">                                
                                        <div class="milestone thumb-down"> 
                                          <div class="status-label"></div> 
                                          <div class="mutual-status complete-rating"> 
                                                <h3><?php echo $_smarty_tpl->getVariable('lang313')->value;?>
</h3> 
                                            </div> 
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->getVariable('o')->value['status']=="5"){?>
                        <div class="db-main-table okay">
                            <table>
                                <thead>
                                    <tr>
                                        <td colspan="2">
                                            <?php echo $_smarty_tpl->getVariable('lang314')->value;?>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                	<td class="leftitscriptolution width25">
                                        <i style="color:#0ABA44" class="fa fa-check-square-o fa-4x"></i>
                                    </td>
                                    <td>  
                                        <div class="complete-order-link"><?php echo $_smarty_tpl->getVariable('lang315')->value;?>
 <a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php $_smarty_tpl->assign('cvseo' , insert_get_seo_convo (array('value' => 'a', 'username' => stripslashes($_smarty_tpl->getVariable('o')->value['username'])),$_smarty_tpl), true);?>"><?php echo $_smarty_tpl->getVariable('lang317')->value;?>
</a></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="7"){?>
                        <div class="db-main-table redbg">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="leftitscriptolution width25">
                                        <i style="color:#FB3737" class="fa fa-times fa-3x"></i> 
                                    </td>
                                    <td class="leftitscriptolution width75">                                
                                        <div class="milestone cancel" title="<?php echo $_smarty_tpl->getVariable('lang359')->value;?>
">
                                          <div class="status-label"></div>
                                          <div class="mutual-status duedate"><span><?php echo $_smarty_tpl->getVariable('lang359')->value;?>
</span></div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>                        
                        <?php }?>
                    </div>
                    <?php if ($_smarty_tpl->getVariable('o')->value['status']=="1"||$_smarty_tpl->getVariable('o')->value['status']=="4"||$_smarty_tpl->getVariable('o')->value['status']=="6"){?>
                    <?php $_template = new Smarty_Internal_Template("track_bit2.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                    <?php }elseif($_smarty_tpl->getVariable('o')->value['status']=="0"){?>
                    <?php $_template = new Smarty_Internal_Template("track_bit.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                    <?php }?>
					<div class="clear"></div>				
				</div>
			</div>
            
            <div class="right-side">
				<center>
                <?php echo insert_get_advertisement(array('AID' => 2),$_smarty_tpl);?>

                </center>	
			</div>
                        
		</div>    
	</div>
</div>