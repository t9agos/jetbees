<?php /* Smarty version Smarty-3.0.7, created on 2017-03-21 22:24:45
         compiled from "/home/marcosta/public_html/themes/scriptolution_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:149268835258d1d25d6e82c8-13320278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f0700a241dc7d7f92fcf48e4b61fd26f1794da18' => 
    array (
      0 => '/home/marcosta/public_html/themes/scriptolution_header.tpl',
      1 => 1490145881,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149268835258d1d25d6e82c8-13320278',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />    
    <title><?php if ($_smarty_tpl->getVariable('mtitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('mtitle')->value;?>
<?php }else{ ?><?php if ($_smarty_tpl->getVariable('pagetitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('pagetitle')->value;?>
 - <?php }?><?php echo $_smarty_tpl->getVariable('site_name')->value;?>
<?php }?></title>
	<meta name="description" content="<?php if ($_smarty_tpl->getVariable('mdesc')->value!=''){?><?php echo $_smarty_tpl->getVariable('mdesc')->value;?>
<?php }else{ ?><?php if ($_smarty_tpl->getVariable('pagetitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('pagetitle')->value;?>
 - <?php }?><?php if ($_smarty_tpl->getVariable('metadescription')->value!=''){?><?php echo $_smarty_tpl->getVariable('metadescription')->value;?>
 - <?php }?><?php echo $_smarty_tpl->getVariable('site_name')->value;?>
<?php }?>">
	<meta name="keywords" content="<?php if ($_smarty_tpl->getVariable('mtags')->value!=''){?><?php echo $_smarty_tpl->getVariable('mtags')->value;?>
<?php }else{ ?><?php if ($_smarty_tpl->getVariable('pagetitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('pagetitle')->value;?>
,<?php }?><?php if ($_smarty_tpl->getVariable('metakeywords')->value!=''){?><?php echo $_smarty_tpl->getVariable('metakeywords')->value;?>
,<?php }?><?php echo $_smarty_tpl->getVariable('site_name')->value;?>
<?php }?>">   
    
    <link rel="canonical" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
<?php echo strtok($_SERVER['REQUEST_URI'],'?');?>
" />
    
    <meta property="fb:app_id" content="<?php echo $_smarty_tpl->getVariable('FACEBOOK_APP_ID')->value;?>
" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php if ($_smarty_tpl->getVariable('mtitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('mtitle')->value;?>
<?php }else{ ?><?php if ($_smarty_tpl->getVariable('pagetitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('pagetitle')->value;?>
 - <?php }?><?php echo $_smarty_tpl->getVariable('site_name')->value;?>
<?php }?>" />
    <meta property="og:description" content="<?php if ($_smarty_tpl->getVariable('mdesc')->value!=''){?><?php echo $_smarty_tpl->getVariable('mdesc')->value;?>
<?php }else{ ?><?php if ($_smarty_tpl->getVariable('pagetitle')->value!=''){?><?php echo $_smarty_tpl->getVariable('pagetitle')->value;?>
 - <?php }?><?php if ($_smarty_tpl->getVariable('metadescription')->value!=''){?><?php echo $_smarty_tpl->getVariable('metadescription')->value;?>
 - <?php }?><?php echo $_smarty_tpl->getVariable('site_name')->value;?>
<?php }?>" />
    <meta property="og:site_name" content="<?php echo $_smarty_tpl->getVariable('site_name')->value;?>
" />
    <meta property="og:url" content="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
<?php echo strtok($_SERVER['REQUEST_URI'],'?');?>
">
    <?php if (isset($_smarty_tpl->getVariable('viewpage',null,true,false)->value)){?>
    <meta property="og:image" content="<?php echo $_smarty_tpl->getVariable('purl')->value;?>
/t3/<?php echo $_smarty_tpl->getVariable('p')->value['p1'];?>
" />
    <meta property="og:image:width" content="678">
    <meta property="og:image:height" content="458">
    <?php }else{ ?>
    <meta property="og:image" content="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/images/opengraph.png" />
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="2048">
    <meta property="og:image:height" content="1075">
    <?php }?>
    
    <script type="text/javascript">
    var base_url = "<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
";
	</script>
    
    
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1020396394701011'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1020396394701011&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    
    <link rel="icon" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/favicon.ico" />
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    
    <style type="text/css">
    	<?php echo file_get_contents(($_smarty_tpl->getVariable('baseurl')->value)."/css/scriptolution_style_v7.css");?>

    	
    	<?php if ($_smarty_tpl->getVariable('scriptolutionhome')->value=="1"){?>
    	<?php echo file_get_contents(($_smarty_tpl->getVariable('baseurl')->value)."/css/scriptolution_style_v7_home.css");?>

    	<?php }?>
    </style>
</head>
<body>


	<script>
	  (function(i,s,o,g,r,a,m)
	  {
		  i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
		  {
	  (i[r].q=i[r].q||[]).push(arguments)
	  }
	  ,i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  }
	  )(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-76203254-1', 'auto');
	  ga('send', 'pageview');
	
	</script>



<div id="loadme"></div>
<?php if ($_smarty_tpl->getVariable('enable_fc')->value=="1"){?>
<div id="fb-root"></div>

<script type="text/javascript">
  window.fbAsyncInit = function() {
		FB.init({
			appId: '<?php echo $_smarty_tpl->getVariable('FACEBOOK_APP_ID')->value;?>
',
			status: true,
			cookie: true,
			xfbml: true
		});
		
		FB.Event.subscribe('auth.login', function(response){});	  
  };

  // Load the SDK Asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<?php }?>
<?php if ($_SESSION['USERID']!=''){?>

<script type="text/javascript">
function loadContent(elementSelector, sourceURL) {
$(""+elementSelector+"").load(""+sourceURL+"");
}
</script>

<?php }?>
<div class="header">
	<div class="centerwrap relative">
    	<div class="headertop">
        	<div class="scriptolutionmenubutton"><a href="javascript:void();" onclick="scriptolution_newtoggle('scriptolutionmobilenav');"><i class="fa fa-bars"></i></a></div>
        	<div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/"><img src="<?php echo $_smarty_tpl->getVariable('imageurl')->value;?>
/scriptolution_logo.png" alt="<?php echo $_smarty_tpl->getVariable('site_name')->value;?>
" /></a></div>
            <div class="search">                
                <form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/search" id="search_form" method="get">
                <input name="query" type="text" class="textbox scriptolutiontextbox20"/>
                <input type="hidden" name="c" id="scriptolution_search_cat" value="0" />
                <input type="submit" value="" class="searchbtn" />
                </form>
            </div>
            <?php $_template = new Smarty_Internal_Template('scriptolution_dotcom_notifications.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
            <?php if ($_SESSION['USERID']!=''){?>
            <div class="logoutheader">
    	  		<div class="usernamebox droparrow">
                	<?php $_smarty_tpl->assign('myprofilepicture' , insert_get_member_profilepicture (array('value' => 'var', 'USERID' => $_SESSION['USERID']),$_smarty_tpl), true);?>
                	<div class="userimage"><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo insert_get_seo_profile(array('value' => 'a', 'username' => stripslashes($_SESSION['USERNAME'])),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('membersprofilepicurl')->value;?>
/thumbs/<?php echo $_smarty_tpl->getVariable('myprofilepicture')->value;?>
" alt="<?php echo $_SESSION['USERNAME'];?>
" height="25px" width="25px" /></a></div>
              		<p><?php echo $_smarty_tpl->getVariable('lang29')->value;?>
</p>
           	  		<div class="clear"></div>
                    <div class="dropdownbox">
                    	<ul>
                        	<?php if ($_smarty_tpl->getVariable('enable_ref')->value=="1"){?><li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/myreferrals"><?php echo $_smarty_tpl->getVariable('lang512')->value;?>
</a></li><?php }?>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/bookmarks"><?php echo $_smarty_tpl->getVariable('lang30')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/settings"><?php echo $_smarty_tpl->getVariable('lang31')->value;?>
</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:loadContent('#loadme', '<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/log_out');"><?php echo $_smarty_tpl->getVariable('lang27')->value;?>
</a></li>
                        </ul>
                    </div>
              	</div>
       	  		<div class="textdropdown sublinks">
               		<p><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_gigs"><?php echo $_smarty_tpl->getVariable('lang156')->value;?>
 <span class="arrow"><img src="<?php echo $_smarty_tpl->getVariable('imageurl')->value;?>
/scriptolution_navdroparrow.png" alt="" /></span></a></p>
                    <div class="dropdownbox">
                    	<ul>
                        	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/new"><?php echo $_smarty_tpl->getVariable('lang55')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_gigs"><?php echo $_smarty_tpl->getVariable('lang153')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_orders"><?php echo $_smarty_tpl->getVariable('lang154')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance?tab=sales"><?php echo $_smarty_tpl->getVariable('lang155')->value;?>
</a></li>
                            <!-- <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/purchases"><?php echo $_smarty_tpl->getVariable('lang461')->value;?>
</a></li> -->
                            <?php $_template = new Smarty_Internal_Template('scriptolution_co_bit1.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> 
                        </ul>
                    </div>
                </div>
          		<div class="textdropdown sublinks">
               		<p><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance"><?php echo $_smarty_tpl->getVariable('lang158')->value;?>
 <span class="arrow"><img src="<?php echo $_smarty_tpl->getVariable('imageurl')->value;?>
/scriptolution_navdroparrow.png" alt="" /></span></a></p>
                    <div class="dropdownbox">
                    	<ul>
                        	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/orders"><?php echo $_smarty_tpl->getVariable('lang157')->value;?>
</a></li>
                        	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/suggested"><?php echo $_smarty_tpl->getVariable('lang595')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/mysuggestions"><?php echo $_smarty_tpl->getVariable('lang511')->value;?>
</a></li>
                            <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance"><?php echo $_smarty_tpl->getVariable('lang159')->value;?>
</a></li>
                            <?php $_template = new Smarty_Internal_Template('scriptolution_co_bit2.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
                        </ul>
                    </div>
                </div>
                <div class="massage"><a href="#" onclick="scriptolutions_fiverrscript_toggleit('scriptolution-notifications-popup');"><img id="scriptolution-master" src="<?php echo $_smarty_tpl->getVariable('imageurl')->value;?>
/scriptolution_chat.png" style="margin-top: 5px;" /></a></div>
                <div class="todo leftborder">
                	<?php $_smarty_tpl->assign('msgc' , insert_msg_cnt (array('value' => 'var'),$_smarty_tpl), true);?>
                	<p><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/inbox"><?php echo $_smarty_tpl->getVariable('lang28')->value;?>
</a> <span><?php echo $_smarty_tpl->getVariable('msgc')->value;?>
</span></p>
                </div>
            	<div class="clear"></div>
          	</div> 
            <?php }else{ ?>
        	<div class="headeright">
            	<ul>
                	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/"><?php echo $_smarty_tpl->getVariable('lang0')->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/login"><?php echo $_smarty_tpl->getVariable('lang2')->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/signup" class="join"><?php echo $_smarty_tpl->getVariable('lang1')->value;?>
</a></li>
                </ul>
            </div>
            <?php }?>
        	<div class="clear"></div>
        </div>
    </div>
	<div class="subnav">
    	<div class="centerwrap">
        	<ul>
            	<?php $_smarty_tpl->assign('c' , insert_get_categories (array(),$_smarty_tpl), true);?>
                <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('c')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)10;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
            	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['seo']);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['name']);?>
</a>
                	<?php $_smarty_tpl->assign('subcat' , insert_get_subcategories (array('value' => 'var', 'parent' => $_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['CATID']),$_smarty_tpl), true);?>
                    <?php if (count($_smarty_tpl->getVariable('subcat')->value)>"0"){?>
                	<div class="menubox">
                    	<div class="menulist">
                        	<ul>
                            	<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['name'] = 'sc';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('subcat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total']);
?>
                            	<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['seo']);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['name']);?>
</a></li>
                                <?php endfor; endif; ?>
                            </ul>
                        </div>
                    </div>
                    <?php }?>
                </li>
                <?php endfor; endif; ?>
                <li class="submenu-right"><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories"><?php echo $_smarty_tpl->getVariable('lang557')->value;?>
</a></li>
            </ul>
        </div>
    </div>
    <?php $_template = new Smarty_Internal_Template('fiverrscript_dotcom_notifications.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
</div>
<div class="nav-scriptolution" id="scriptolutionmobilenav" style="display:none">
	<div class="scriptolution-dotcom-mobile-dropdown" id="dropdown-menu">
    	<div class="scriptolutionclose-nav" onclick="scriptolution_newtoggle('scriptolutionmobilenav');">×</div>
        <div class="searchforscriptolutionmobleonly">
        	<form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/search" method="get">
            <input name="query" type="text" class="textbox scriptolutiontextbox18 scriptolutionwidth80"/>
            <input type="submit" value="<?php echo $_smarty_tpl->getVariable('lang504')->value;?>
" class="ascriptolutiongreenbutton" style="border: 0px;" />
            </form>
        </div>
        <hr>
        <?php if ($_SESSION['USERID']!=''){?>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/inbox"><?php echo $_smarty_tpl->getVariable('lang28')->value;?>
 <span><?php echo $_smarty_tpl->getVariable('msgc')->value;?>
</span></a>
        <hr>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/notifications"><?php echo $_smarty_tpl->getVariable('lang544')->value;?>
</a>
        <hr>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance"><?php echo $_smarty_tpl->getVariable('lang158')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/orders"><?php echo $_smarty_tpl->getVariable('lang157')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/mysuggestions"><?php echo $_smarty_tpl->getVariable('lang511')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance"><?php echo $_smarty_tpl->getVariable('lang159')->value;?>
</a>
        <?php $_template = new Smarty_Internal_Template('scriptolution_co_bit2_responsive.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <hr>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_gigs"><?php echo $_smarty_tpl->getVariable('lang156')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/new"><?php echo $_smarty_tpl->getVariable('lang55')->value;?>
</a></li>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_gigs"><?php echo $_smarty_tpl->getVariable('lang153')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_orders"><?php echo $_smarty_tpl->getVariable('lang154')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance?tab=sales"><?php echo $_smarty_tpl->getVariable('lang155')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/purchases"><?php echo $_smarty_tpl->getVariable('lang461')->value;?>
</a>
        <?php $_template = new Smarty_Internal_Template('scriptolution_co_bit1_responsive.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <hr>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/<?php echo insert_get_seo_profile(array('value' => 'a', 'username' => stripslashes($_SESSION['USERNAME'])),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->getVariable('lang29')->value;?>
</a>
        <?php if ($_smarty_tpl->getVariable('enable_ref')->value=="1"){?><a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/myreferrals"><?php echo $_smarty_tpl->getVariable('lang512')->value;?>
</a><?php }?>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/bookmarks"><?php echo $_smarty_tpl->getVariable('lang30')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/settings"><?php echo $_smarty_tpl->getVariable('lang31')->value;?>
</a>
        <a class="scriptolutionfiverrscriptitem" href="javascript:loadContent('#loadme', '<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/log_out');"><?php echo $_smarty_tpl->getVariable('lang27')->value;?>
</a>
        <?php }else{ ?>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/"><?php echo $_smarty_tpl->getVariable('lang0')->value;?>
</a>
        <hr>
        <a class="scriptolutionfiverrscriptitem" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/login"><?php echo $_smarty_tpl->getVariable('lang2')->value;?>
</a>
        <hr>
        <a class="scriptolutionfiverrscriptitem join" href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/signup"><?php echo $_smarty_tpl->getVariable('lang1')->value;?>
</a>
        <?php }?>
	</div>
</div>
<?php if ($_smarty_tpl->getVariable('scriptolutionhome')->value=="1"){?>
<?php if ($_SESSION['USERID']==''){?>
<div class="banner">
    <div class="centerwrap relative">
    	<div class="bannertext">
        	<h3><?php echo $_smarty_tpl->getVariable('lang21')->value;?>
 <?php echo $_smarty_tpl->getVariable('site_name')->value;?>
</h3>
            <h2><?php echo $_smarty_tpl->getVariable('lang102')->value;?>
<br /><?php echo $_smarty_tpl->getVariable('lang103')->value;?>
</h2>
            <div class="find-service">                
                <form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/search" method="get">
                <input name="query" type="text" class="findbox"/>
                <input type="submit" value="<?php echo $_smarty_tpl->getVariable('lang556')->value;?>
" class="findbtn"/>
                </form>
                <div class="clear"></div>
            </div>
            <p><a class='inline' href="#inline1" title="<?php echo $_smarty_tpl->getVariable('lang419')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang419')->value;?>
</a></p>
            <div class="topnavbg"></div>
        </div>
        <div class="scriptolutionmobilecats">
            <div class="btn-list-space">
                <select class="btn outline-only white" onchange="javascript:location.href = this.value;">
                    <option value=""><?php echo $_smarty_tpl->getVariable('lang524')->value;?>
</option>
                    <?php $_smarty_tpl->assign('c' , insert_get_categories (array(),$_smarty_tpl), true);?>
                    <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('c')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                    <option value="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['seo']);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['name']);?>
</option>
                    <?php $_smarty_tpl->assign('subcat' , insert_get_subcategories (array('value' => 'var', 'parent' => $_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['CATID']),$_smarty_tpl), true);?>
                    <?php if (count($_smarty_tpl->getVariable('subcat')->value)>"0"){?>
                        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['name'] = 'sc';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('subcat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total']);
?>
                        <option value="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['seo']);?>
">- <?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['name']);?>
</option>
                        <?php endfor; endif; ?>
                    <?php }?>
                    <?php endfor; endif; ?>
                </select>
            </div>
        </div>
    </div>
</div>
<?php $_template = new Smarty_Internal_Template('scriptolution_colorbox2.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<?php }else{ ?>
		<div class="scriptolutionmobilecats">
            <div class="btn-list-space <?php if ($_SESSION['USERID']!=''){?>scriptolutionaddwhitebg<?php }?>">
                <select class="btn outline-only white" onchange="javascript:location.href = this.value;">
                    <option value=""><?php echo $_smarty_tpl->getVariable('lang524')->value;?>
</option>
                    <?php $_smarty_tpl->assign('c' , insert_get_categories (array(),$_smarty_tpl), true);?>
                    <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('c')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                    <option value="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['seo']);?>
"><?php echo stripslashes($_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['name']);?>
</option>
                    <?php $_smarty_tpl->assign('subcat' , insert_get_subcategories (array('value' => 'var', 'parent' => $_smarty_tpl->getVariable('c')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['CATID']),$_smarty_tpl), true);?>
                    <?php if (count($_smarty_tpl->getVariable('subcat')->value)>"0"){?>
                        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['name'] = 'sc';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('subcat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sc']['total']);
?>
                        <option value="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/categories/<?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['seo']);?>
">- <?php echo stripslashes($_smarty_tpl->getVariable('subcat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sc']['index']]['name']);?>
</option>
                        <?php endfor; endif; ?>
                    <?php }?>
                    <?php endfor; endif; ?>
                </select>
            </div>
            <div class="scriptolution60"></div>
        </div>
<?php }?>
<?php }?>