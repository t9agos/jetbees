<?php /* Smarty version Smarty-3.0.7, created on 2015-10-03 17:23:25
         compiled from "/home/marcosta/public_html/themes/bookmarks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6141949235610474d2b6c50-87302650%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76182b18a67d5d946aa9854679af66ae7bbb968f' => 
    array (
      0 => '/home/marcosta/public_html/themes/bookmarks.tpl',
      1 => 1443800324,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6141949235610474d2b6c50-87302650',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/mainscriptolution.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.qtip-1.0.0-rc3.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/jquery.corner.js" type="text/javascript"></script> 
<script src="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/js/indexes.js" type="text/javascript"></script> 

<style>
.songslist h1 {
  float: left;
  width: 100%;
  font-size: 39px;
  font-family: 'latobold', sans-serif;
  color: #424242;
  margin-bottom: 15px;
}
</style>

<?php $_template = new Smarty_Internal_Template("scriptolution_error7.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<div class="bodybg scriptolutionpaddingtop15 scriptolutionbookmarks">
	<div class="whitebody scriptolutionpaddingtop30 gray">
		<div class="inner-wrapper">
			<div class="clear"></div>
			<div class="left-side">

                <div class="songslist">
                    <h1><?php echo $_smarty_tpl->getVariable('lang30')->value;?>
</h1>
                    <div class="cusongslist">
                        <?php $_template = new Smarty_Internal_Template("scriptolution_bit_last3_my.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>                
                        <div class="clear"></div>
                    </div>
                    <div align="center">
                        <div class="paging">
                            <div class="p1">
                                <ul>
                                    <?php echo $_smarty_tpl->getVariable('pagelinks')->value;?>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="clear" style="padding-bottom:20px;"></div>
                </div>
  
			</div>
			<div class="right-side">
				<div class="sidebarBlock">
					<h3><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('lang577')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('lang577')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('lang577')->value));?>
</h3>
					<ul>
						<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/new" class="greenBtn"><span><?php echo $_smarty_tpl->getVariable('lang55')->value;?>
</span></a></li>
					</ul>
				</div>
				<div class="sidebarBlock">
					<h2><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('lang33')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('lang33')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('lang33')->value));?>
</h2>
					<ul>
						<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_gigs"><?php echo $_smarty_tpl->getVariable('lang153')->value;?>
</a></li>
                        <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/manage_orders"><?php echo $_smarty_tpl->getVariable('lang154')->value;?>
</a></li>
					</ul>
				</div>
				<div class="sidebarBlock">
					<h3><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('lang578')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('lang578')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('lang578')->value));?>
</h3>
					<ul>
						<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/balance?tab=sales"><?php echo $_smarty_tpl->getVariable('lang155')->value;?>
</a></li>
                        <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/purchases"><?php echo $_smarty_tpl->getVariable('lang461')->value;?>
</a></li>
					</ul>
				</div>
                <div class="sidebarBlock noBorder">
					<h3><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('lang496')->value, 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('lang496')->value,SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('lang496')->value));?>
</h3>
					<ul>
						<li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/mysuggestions"><?php echo $_smarty_tpl->getVariable('lang511')->value;?>
</a></li>
                        <li><a href="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/suggested"><?php echo $_smarty_tpl->getVariable('lang579')->value;?>
</a></li>
					</ul>
				</div>				
			</div>
		</div>    
	</div>
</div>