<?php /* Smarty version Smarty-3.0.7, created on 2015-10-08 09:07:42
         compiled from "/home/marcosta/public_html/themes/track_bit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:143360740356166a9e0cfca4-52562221%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '978e60e541c635e9ecd31e157c918d4737c4ca5d' => 
    array (
      0 => '/home/marcosta/public_html/themes/track_bit.tpl',
      1 => 1443800331,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '143360740356166a9e0cfca4-52562221',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div style="clear:both !important"></div>
<form action="<?php echo $_smarty_tpl->getVariable('baseurl')->value;?>
/send_track" class="nonajaxy" id="new_message" method="post">
<input type="hidden" name="submg" value="1" />
<input type="hidden" name="msgto" value="<?php echo $_smarty_tpl->getVariable('o')->value['owner'];?>
" />
<input type="hidden" name="oid" value="<?php echo $_smarty_tpl->getVariable('o')->value['OID'];?>
" />
<div class="content-text">
<div class="msg-error"><p id="message_validation_error"></p></div>
    <div class="conv-write">
        <header class="cf">
        	<?php if ($_smarty_tpl->getVariable('o')->value['status']=="0"){?> <label class="js-message-label" style="color:#049BCF"><?php echo $_smarty_tpl->getVariable('lang274')->value;?>
</label><?php }?>
        </header>
        <div class="write-wrap">
            <div class="write-row cf">
                <textarea maxlength="1200" class="no-min-len" cols="75" id="message_body" name="message_body" rows="3"></textarea>
                <input id="message_message_format" name="message_message_format" type="hidden" /> 
                <input type="hidden" name="who" value="buyer" />
            </div>
            <div class="message-form-control">
                <div class="write-controls cf">
                    <div class="send-button"> 
                        <div class="icn-submit"><input class="btn-standard btn-send-message send-small" name="commit" value="<?php echo $_smarty_tpl->getVariable('lang46')->value;?>
" type="submit"></div>
                        <div class="progress-indicator-icon-message" style="display:none"> 
                            <span class="sending"><?php echo $_smarty_tpl->getVariable('lang120')->value;?>
</span> 
                        </div>
                    </div>
                    
                    <div class="attach-files">
                        <div class="attach-inner">
                            <div id="toggle-attach">
                                <input id="message_message_attachment_id" name="message_message_attachment_id" type="hidden" />
                                <input id="fileInput" name="fileInput" type="file" />                                             
                            </div>
                        </div>
                        <small class="attach-limit"></small>
                    </div>
                </div>
            </div>  
        </div>
        <div style="padding:10px;">
            <div class="files-added"> 
                <span id="attached_file_name"></span> <b class="remove_attachment" style="cursor:pointer">x</b> 
            </div>
            <div style="clear:both"></div>
            <div style="color:#666; font-size:12px"><b><?php echo $_smarty_tpl->getVariable('lang255')->value;?>
</b>: <?php echo $_smarty_tpl->getVariable('lang256')->value;?>
</div>
        </div>
    </div>	
</div>				
</form>                